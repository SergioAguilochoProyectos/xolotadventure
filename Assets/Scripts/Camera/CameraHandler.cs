using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHandler : MonoBehaviour
{
    [Header("Transforms")]
    public Transform targetTransform;
    public Transform cameraTransform;
    public Transform pivotTransform;
    private Transform myTransform;

    private Vector3 cameraTransformPosition;
    private LayerMask ignoreLayer;

    public static CameraHandler instance;

    [Header("Speed")]
    public float lookSpeed;
    public float followSpeed;
    public float pivotSpeed;

    [Header("PivotSettings")]
    private float defaultPosition;
    private float lookAngle;
    private float pivotAngle;
    public float minimumPivot;
    public float maximumPivot;

    private void Awake()
    {
        instance = this;
        myTransform = transform;
        defaultPosition = cameraTransform.localPosition.z;
        ignoreLayer = ~(1 << 8 | 1 << 9 | 1 << 10);
    }

    private void Update()
    {
        
    }

    public void FollowTarget(float delta)
    {
        Vector3 targetPosition = Vector3.Lerp(myTransform.position, targetTransform.position, delta / followSpeed);

        myTransform.position = targetPosition;
    }

    public void HandleCameraRotation(float delta, float mouseXInput, float mouseYInput)
    {
        lookAngle += (mouseXInput * lookSpeed) / delta;
        pivotAngle -= (mouseYInput * pivotSpeed) / delta;
        pivotAngle = Mathf.Clamp(pivotAngle, minimumPivot, maximumPivot);
        //pivotAngle = Mathf.Lerp(minimumPivot, maximumPivot, pivotAngle);

        Vector3 rotation = Vector3.zero;
        rotation.y = lookAngle;
        Quaternion targetRotation = Quaternion.Euler(rotation);
        myTransform.rotation = targetRotation;

        rotation = Vector3.zero;
        rotation.x = pivotAngle;

        targetRotation = Quaternion.Euler(rotation);
        pivotTransform.localRotation = targetRotation;

    }
}
