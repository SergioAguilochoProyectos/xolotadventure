using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraShake : MonoBehaviour
{
    public static CameraShake Instance { get; private set; }

    public CinemachineVirtualCamera followCamera;
    public CinemachineVirtualCamera targetCamera;

    private float shakeTimer;

    private void Awake()
    {
        Instance = this;

    }

    private void Update()
    {
        if(shakeTimer > 0)
        {
            shakeTimer -= Time.deltaTime;

            if(shakeTimer <= 0f)
            {
                CinemachineBasicMultiChannelPerlin followCameraNoise =
                followCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
                CinemachineBasicMultiChannelPerlin targetCameraNoise =
                targetCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

                followCameraNoise.m_AmplitudeGain = 0f;
                targetCameraNoise.m_AmplitudeGain = 0f;
            }
        }
    }

    /// <summary>
    /// Realiza las funciones para agitar la c�mara
    /// </summary>
    /// <param name="intensity"></param>
    /// <param name="time"></param>
    public void ShakeCamera(float intensity, float time)
    {
        Debug.Log("Shake");


        CinemachineBasicMultiChannelPerlin followCameraNoise =
        followCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        CinemachineBasicMultiChannelPerlin targetCameraNoise =
        targetCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();


        followCameraNoise.m_AmplitudeGain = intensity;
        targetCameraNoise.m_AmplitudeGain = intensity;
        shakeTimer = time;
    }
}
