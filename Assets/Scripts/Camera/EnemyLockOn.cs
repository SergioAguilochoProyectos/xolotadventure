using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.SceneManagement;

public class EnemyLockOn : MonoBehaviour
{
    public static EnemyLockOn instance;

    [SerializeField] private Transform currentTarget;
    public bool showGizmos;

    public LayerMask targetLayer;
    public Transform enemyTarget;

    public CinemachineVirtualCamera followCamera;
    public CinemachineVirtualCamera targetCamera;

    public GameObject cameraTargetObject;

    //[Header("Settings")]
    public bool look;
    [SerializeField] private float range;
    [SerializeField] private float lookAt;
    [SerializeField] private float maxNoticeAngle;
    [SerializeField] private float scale;

    private Transform cam;
    public bool enemyLocked;
    public float currentYOffset;
    private Vector3 pos;

    public CameraFollow camfollow;
    public Transform lockOnCanvas;

    private PlayerController player;

    public bool canLock;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<PlayerController>();
        cam = Camera.main.transform;

        CameraFollow.Instance.lockedTarget = false;

        if(lockOnCanvas != null)
        {
            lockOnCanvas.gameObject.SetActive(false);
        }
        
        canLock = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (DataManager.instance.data.missions[2].isDone)
        {
            canLock = true;
        }

        if (canLock)
        {
            if (Input.GetButtonDown("LockTarget"))
            {
                if (currentTarget)
                {
                    //If there is already a target, Reset.
                    ResetTarget();
                    return;
                }
                if (currentTarget = ScanNearBy())
                {
                    FoundTarget();
                }
                else
                {
                    ResetTarget();
                }

            }
        }

        if (SceneManager.GetActiveScene().name == "FireCamp 1")
        {
            canLock = true;
        }


        if (enemyLocked)
        {
            if (!TargetOnRange())
            {
                ResetTarget();
            }

            LookAtTarget();

        }

    }

    /// <summary>
    /// Registra un objetivo a fijar
    /// </summary>
    private void FoundTarget()
    {
        Debug.Log("Locking");
        lockOnCanvas.gameObject.SetActive(true);
        enemyLocked = true;

        followCamera.enabled = false;
        targetCamera.enabled = true;

        CameraFollow.Instance.lockedTarget = true;

    }


    /// <summary>
    /// Resetea el objetivo fijado
    /// </summary>
    public void ResetTarget()
    {
        
        followCamera.enabled = true;
        targetCamera.enabled = false;

        lockOnCanvas.gameObject.SetActive(false);
        enemyLocked = false;
        currentTarget = null;

        CameraFollow.Instance.lockedTarget = false;

    }


    private Transform ScanNearBy()
    {
        Collider[] nearByTargets = Physics.OverlapSphere(transform.localPosition, range, targetLayer);

        float closestAngle = maxNoticeAngle;
        Transform closestTarget = null;

        if (nearByTargets.Length <= 0) return null;

        for (int i = 0; i < nearByTargets.Length; i++)
        {
            Vector3 dir = nearByTargets[i].transform.position - cam.position;
            dir.y = 0;
            float _angle = Vector3.Angle(cam.forward, dir);

            if(_angle < closestAngle)
            {
                closestTarget = nearByTargets[i].transform;
                closestAngle = _angle;
            }
        }

        if (!closestTarget) return null;


        float h1 = closestTarget.GetComponent<CapsuleCollider>().height;
        float h2 = closestTarget.localScale.y;
        float h = h1 * h2;
        float half_h = (h / 2) / 2;

        currentYOffset = h - half_h;

        if (look && currentYOffset > 1.6f && currentYOffset < 1.6f * 3) currentYOffset = 1.6f;

        Vector3 tarPos = closestTarget.position + new Vector3(0, currentYOffset, 0);

        if (Blocked(tarPos)) return null;

        //if (!TargetOnRange()) return null;

        return closestTarget;

    }

    /// <summary>
    /// Interpreta si la visi�n del target est� siendo bloqueada por alg�n obst�culo
    /// </summary>
    /// <returns></returns>
    private bool Blocked(Vector3 tarLoc)
    {
        RaycastHit hit;

        if (Physics.Linecast(transform.position + Vector3.up * 0.5f, tarLoc, out hit))
        {
            if (!hit.transform.CompareTag("Enemy") && !hit.transform.CompareTag("Boss")) return true;
        }

        return false;
    }

    /// <summary>
    /// Calcula si la distancia es demasiado grande para fijar al objetivo
    /// </summary>
    /// <returns></returns>
    private bool TargetOnRange()
    {
        float dis = (transform.position - pos).magnitude;
        if (dis > range) return false; else return true;
    }

    /// <summary>
    /// 
    /// </summary>
    private void LookAtTarget()
    {
        if (currentTarget == null)
        {
            ResetTarget();
            return;
        }
        pos = currentTarget.position;
        lockOnCanvas.position = pos + new Vector3(0, 1, 0);
        lockOnCanvas.localScale = Vector3.one * ((cam.position - pos).magnitude * scale);

        enemyTarget.position = pos;
        Vector3 dir = currentTarget.position - transform.position;
        dir.y = 0;
        Quaternion rot = Quaternion.LookRotation(dir);
        transform.rotation = Quaternion.Lerp(transform.localRotation, rot, Time.deltaTime * lookAt);
    }



    private void OnDrawGizmos()
    {
        if (!showGizmos) return;

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
    
}
