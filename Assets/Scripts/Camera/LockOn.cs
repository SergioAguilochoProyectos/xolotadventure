using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class LockOn : MonoBehaviour
{
    public bool gizmosOn;
    public Transform lockRangeCenter;
    public float lockOnRange;
    public LayerMask lockOnLayer;
    //public CinemachineFreeLook freeLook;
    public CinemachineBrain brain;
    public CinemachineTargetGroup targetGroup;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update()
    {
        CollectLockOnObjects();
    }

    private void OnDrawGizmos()
    {
        if (!gizmosOn) return;

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(lockRangeCenter.position, lockOnRange);
    }

    /*
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Enemy"))
        {
            //targetGroup.m_Targets[1].target = other.gameObject.transform;
            //other.gameObject = targetGroup.m_Targets[1].target;
        }
    }
    */

    
    /// <summary>
    /// Recoge los gameobjects que hacen lockOn
    /// </summary>
    private void CollectLockOnObjects()
    {
        Collider[] hitColliders = Physics.OverlapSphere(lockRangeCenter.position, lockOnRange, lockOnLayer);

        foreach (BoxCollider collider in hitColliders)
        {
            Debug.Log("Enemy");
            targetGroup.m_Targets[1].target = collider.gameObject.transform;

        }

    }
    
}
