using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class CheckMission : MonoBehaviour
{
    [Header("Mission")]
    public string missionID;
    public int amount;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            MissionManager.instance.CheckMission(missionID, amount);
        }
    }
}
