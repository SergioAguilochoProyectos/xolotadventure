using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckScene : MonoBehaviour
{
    public string sceneName;
    public string entrancePosition;
    [Header("Unlock Travelling")]
    public int missionNumber;
    private void OnTriggerEnter(Collider other)
    {
        string scene = sceneName;

        if (other.gameObject.CompareTag("Player"))
        {
            if (sceneName == scene)
            {
                if (DataManager.instance.data.missions[missionNumber].isDone)
                {
                    SceneController.instance.FadeAndLoadScene(sceneName, entrancePosition);
                }
            }
        }

    }
}
