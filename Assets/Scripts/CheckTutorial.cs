using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckTutorial : MonoBehaviour
{
    [Header("Texto")]
    public string textID;
    public Color textColor = Color.black;
    public string characterName;
    public float textDuration;

    public Animator playerAnim;

    public GameObject buttonToPress;
    public Animator animButton;
 
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //StartCoroutine(DelayForText());

            ShowMessage();

            Debug.Log("ENTRA");
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {

            if (Input.GetButtonDown("Interact"))
            {
                TextManager.instance.DisplayMessage(TranslateManager.instance.GetString(textID), textColor, characterName);

                playerAnim.SetFloat("Forward", 0);
                playerAnim.SetFloat("Right", 0);

                HideMessage();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            HideMessage();
            TextManager.instance.HideMessage();
        }
    }
    private void ShowMessage()
    {
        buttonToPress.SetActive(true);
        animButton.SetBool("Press", true);
    }

    private void HideMessage()
    {
        buttonToPress.SetActive(false);
        animButton.SetBool("Press", false);
    }
    /*
    private IEnumerator DelayForText()
    {
        yield return new WaitForSeconds(textDuration);

        TextManager.instance.HideMessage();

        PlayerController.instance.canMove = true;

        if (DataManager.instance.data.missions[0].isDone)
        {
            PlayerController.instance.isRunning = false;
        }

        if (DataManager.instance.data.missions[1].isDone)
        {
            PlayerAttack.instance.canAttack = true;
        }

        if (DataManager.instance.data.missions[2].isDone)
        {
            EnemyLockOn.instance.canLock = true;
        }

        DataManager.instance.CheckForBoxTutorial(boxId, true);

        gameObject.SetActive(false);
    }*/

}
