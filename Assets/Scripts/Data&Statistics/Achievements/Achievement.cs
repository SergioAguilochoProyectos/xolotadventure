[System.Serializable]
public class Achievement
{

    public string name; //Nombre del logro
    public string statCode; //Estadística a verificar
    public string imageName; //Nombre del Sprite del logro
    public string description; //Descripción del logro
    public int targetAmount; //Cantidad a conseguir para obtener el logro
    public bool targetUnlocked = false; //Controlar si el logro ya ha sido desbloqueado

}
