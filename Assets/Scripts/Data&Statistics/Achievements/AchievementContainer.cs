using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Linq;
using UnityEngine.UI;

public class AchievementContainer : MonoBehaviour
{
    public string achievementName;
    public Image image;
    public Text name;
    public Text description;
    public GameObject shadow;

    void Start()
    {

        Achievement result = DataManager.instance.data.achievements.Where(a => a.name == achievementName).FirstOrDefault();

        image.sprite = Resources.Load<Sprite>(result.imageName);
        name.text = result.name;
        description.text = result.description;
        //Si no est� bloqueado se va a activar
        shadow.SetActive(!result.targetUnlocked);

    }
}