using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementUI : MonoBehaviour
{
    #region VARIABLES
    //Imagen del logro
    public Image imageUI;
    //Nombre del logro
    public Text nameUI;

    private Animator anim;
    #endregion
    #region UNITY EVENTS
    private void OnEnable()
    {
        //Si nose ha instanciado, NI implementado la clase AchievementManager
        AchievementManager.OnAchievementUnlock += SetAndShow;
    }

    private void OnDisable()
    {
        AchievementManager.OnAchievementUnlock -= SetAndShow;
    }
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    #endregion
    #region METHODS
    /// <summary>
    /// Modifica los valores de imagen y texto para mostrarlos por pantalla
    /// </summary>
    /// <param name="name"></param>
    /// <param name="imageName"></param>
    public void SetAndShow(string name, string imageName)
    {
        //Carga la imagen en la carpeta RESOURCES, utilizando su nombre
        imageUI.sprite = Resources.Load<Sprite>(imageName);
        nameUI.text = name;

        anim.SetTrigger("Show");
    }
    #endregion
}
