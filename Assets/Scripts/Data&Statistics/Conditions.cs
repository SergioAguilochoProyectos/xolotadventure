using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Conditions
{
    //Nombre de la condici�n, IDENTIFICADOR = �nico
    public string name;
    //Descripci�n de apoyo
    [TextArea]
    public string description;
    //Controlar si ha sido cumplida la condici�n
    public bool done;
}
