using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Data
{
    //Listado de estad�sticas
    public Stat[] statistics;
    //Listado de logros
    public Achievement[] achievements;
    // Listado de misiones
    public Mission[] missions;
    // N�mero de misi�n por el que se va
    public int missionCounter;
    // Nombre de la escena actual
    public string currentScene = "BlockingScene";
    // Nombre del punto de entrada en la escena
    public string entrancePosition = "ExteriorDoor";
    // Array con el estado de todas las condiciones
    public Conditions[] allConditions;
    // Array con el estado de todos los objetos del juego
    public BinaryItem[] allItems;
    // Array con el inventario actual del usuario, inicializado por ahora a 6 elementos
    public BinaryItem[] inventory = new BinaryItem[6];
    // Array con el estado de todos los enemigos del juego
    public EnemyData[] enemyData;
    // Cantidad de dinero que se tiene en este momento
    public int moneyCount;
    // Cantidad de vida que tiene el jugador en este momento
    public int playerHealth;
    // Cantidad de flechas m�ximas de jugador
    public int maxArrow;
    // Cantidad de pociones de jugador
    public int potionAmount;
}
