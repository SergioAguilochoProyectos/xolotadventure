[System.Serializable]
public class EnemyData
{
    // Identificador de enemigo en juego
    public int enemyId;
    // Descripción para nosotros, los programadores
    public bool isDead;
}
