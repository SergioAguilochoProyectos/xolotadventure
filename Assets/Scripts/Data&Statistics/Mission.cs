using UnityEngine;
using UnityEngine.UI;
[System.Serializable]
public class Mission
{
    public static Mission instance;

    

    public string missionID;
    [TextArea]
    public string description;
    public int targetAmount;
    public bool isDone;
}
