using JetBrains.Annotations;
using Newtonsoft.Json.Bson;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.VFX;

public class Boss : MonoBehaviour, IDamageable
{
    #region Variables
    [Header("Boss Stats")]
    public EnemyStats enemy;

    [Header("Engage Settings")]
    public int engageDistance;
    public Transform target;
    private bool engaged;

    [Header("Boss Settings")]
    public Animator abilityAreasAnimator;
    private AnimationClip bladestormAnimationClip;
    private Animator animator;
    private bool isDead;
    public float currentHealth;
    private float healthToSecondPhase;
    private float enemySpeed;
    private NavMeshAgent navMeshAgent;
    private CapsuleCollider capsuleCollider;
    public Collider maceStrikeCollider, chargeCollider, bladestormCollider;
    public GameObject key;
    

    [Header("Boss Attacks")]
    public float timeBetweenAttacks;
    public float maceStrikeCooldown;
    public float chargeAttackCooldown;
    public float chargeDistance;
    public float bladestormAttackCooldown;
    private float canAttackTimer;
    private float maceStrikeTimer;
    private float chargeAttackTimer;
    private float bladeStormAttackTimer;
    public bool isSecondPhase;
    private bool isCasting;
    private bool canAttack;
    private bool ableMaceStrike;
    private bool ableChargeAttack;
    private bool ableBladestormAttack;
    private BossEffects bossEffects;

    [Header("UI Settings")]
    public GameObject healthPanel;
    public Image healthBarImage;

    [Header("Audio Settings")]
    public AudioClip bossDamagedClip;
    public AudioClip bossDyingClip;
    public AudioClip bladestormClip, chargeClip, maceStrikeClip;
    private AudioSource audioSource;
    public AudioSource damagedAudioSource;
    #endregion

    #region Unity Events
    private void Awake() {
        navMeshAgent = GetComponent<NavMeshAgent>();    
    }

    private void Start() {
        animator = GetComponent<Animator>();
        capsuleCollider = GetComponent<CapsuleCollider>();
        audioSource = GetComponent<AudioSource>();
        bossEffects = GetComponent<BossEffects>();
        currentHealth = enemy.maxHealth;
        ableMaceStrike = true;
        ableChargeAttack = true;
        canAttack = true;
        enemySpeed = navMeshAgent.speed;
        healthToSecondPhase = enemy.maxHealth - Random.Range(400, 600);
        foreach (AnimationClip animationClip in animator.runtimeAnimatorController.animationClips) {
            if (animationClip.name.Contains("360")) bladestormAnimationClip = animationClip;
        }
    }

    private void Update() {
        // Si no se ha iniciado el combate �nicamente se comprueba que la distancia con el jugador
        if (!engaged) {
            Engage();
            return; 
        }
        // Se actualiza la barra barra de salud del enemigo
        UpdateHealthBar();
        // Actualiza los temporizadores internos
        UpdateTimers();
        // Si la vida actual del enemigo es inferior o igual al umbral fijado se inicia la segunda fase con el casteo de la habilidad nueva
        if (currentHealth <= healthToSecondPhase && !isSecondPhase) CastBladestorm();
        // Si el enemigo no est� casteando y no est� muerto significa que se puede mover hacia el jugador
        if (!isCasting && !isDead) Movement();
        // Si el enemigo no est� casteando y no est� muerto significa que puede decidir un ataque
        if (!isCasting && !isDead && canAttack) DecideNextAttack();
    }

    #endregion

    #region Methods

    /// <summary>
    /// Decide el siguiente ataque a realizar
    /// </summary>
    public void DecideNextAttack() {
        if (Vector3.Distance(transform.position, target.position) <= navMeshAgent.stoppingDistance) {
            if (!isSecondPhase) {
                switch (Random.Range(1, 3)) {
                    case 1:
                        if (ableMaceStrike) CastMaceStrike();
                        break;
                    case 2:
                        if (ableChargeAttack) CastChargeAttack();
                        break;
                }
            } else {
                switch (Random.Range(1, 4)) {
                    case 1:
                        if (ableMaceStrike) CastMaceStrike();
                        break;
                    case 2:
                        if (ableChargeAttack) CastChargeAttack();
                        break;
                    case 3:
                        if (ableBladestormAttack) CastBladestorm();
                        break;
                }
            }
        }
    }

    public void CastBladestorm() {
        if (!isSecondPhase) isSecondPhase = true;
        // Se recorren todos las animaciones que contiene el AnimatorController
        foreach (AnimationClip animationClip in abilityAreasAnimator.runtimeAnimatorController.animationClips) {
            // Si el nombre del clip coincide con el par�metro
            if (animationClip.name.Contains("Bladestorm")) {
                // Se inicia la corrutina para gestionar el boolean isCasting
                StartCoroutine(WaitForCasting(animationClip.length));
                StartCoroutine(BladestormAttack(animationClip.length));
                ableBladestormAttack = false;
                bladeStormAttackTimer = 0f;
                abilityAreasAnimator.SetTrigger("Bladestorm");
                return;
            }
        }
    }

    /// <summary>
    /// Castea el ataque de maza
    /// </summary>
    public void CastMaceStrike() {
        // Se recorren todos las animaciones que contiene el AnimatorController
        foreach (AnimationClip animationClip in abilityAreasAnimator.runtimeAnimatorController.animationClips) {
            // Si el nombre del clip coincide con el par�metro
            if (animationClip.name.Contains("Mace")) {
                // Se inicia la corrutina para gestionar el boolean isCasting
                animator.SetTrigger("MaceStrike");
                StartCoroutine(WaitForCasting(animationClip.length));
                ableMaceStrike = false;
                maceStrikeTimer = 0f;
                abilityAreasAnimator.SetTrigger("MaceStrike");
                return;
            }
        }
    }

    /// <summary>
    /// Castea la carga frontal
    /// </summary>
    public void CastChargeAttack() {
        // Se recorren todos las animaciones que contiene el AnimatorController
        foreach (AnimationClip animationClip in abilityAreasAnimator.runtimeAnimatorController.animationClips) {
            // Si el nombre del clip coincide con el par�metro
            if (animationClip.name.Contains("Charge")) {
                // Se inicia la corrutina para gestionar el boolean isCasting
                StartCoroutine(WaitForCasting(animationClip.length + 3));
                ableChargeAttack = false;
                chargeAttackTimer = 0f;
                abilityAreasAnimator.SetTrigger("Charge");
                StartCoroutine(ChargeAttack(animationClip.length));
                animator.SetTrigger("Charge");
                return;
            }
        }
    }

    public IEnumerator ChargeAttack(float castingTime) {
        float defaultSpeed = navMeshAgent.speed;
        navMeshAgent.speed *= 2;
        yield return new WaitForSeconds(castingTime);
        navMeshAgent.destination = transform.position + transform.forward * chargeDistance;
        // Tiempo de espera para recuperar su velocidad normal
        yield return new WaitForSeconds(castingTime);
        navMeshAgent.speed = defaultSpeed;
    }

    public IEnumerator BladestormAttack(float castingTime) {
        animator.SetBool("Bladestorm", true);
        bossEffects.tornadoVFX.GetComponentInChildren<VisualEffect>().Play();
        yield return new WaitForSeconds(castingTime);
        animator.SetBool("Bladestorm", false);
        bossEffects.tornadoVFX.GetComponentInChildren<VisualEffect>().Stop();
    }

    public void UpdateTimers() {
        // Si el boolean est� en false se a�ade al temporizador el tiempo que pasa
        if (!canAttack) {
            canAttackTimer += Time.deltaTime;
            // Si el temporizador es igual o superior significa que ya se puede atacar
            if (canAttackTimer >= timeBetweenAttacks) canAttack = true;
        }

        // Si el boolean est� en false se a�ade al temporizador el tiempo que pasa
        if (!ableMaceStrike) {
            maceStrikeTimer += Time.deltaTime;
            // Si el temporizador es igual o superior significa que ya se puede utilizar otra vez
            if (maceStrikeTimer >= maceStrikeCooldown) ableMaceStrike = true;
        }

        // Si el boolean est� en false se a�ade al temporizador el tiempo que pasa
        if (!ableChargeAttack) {
            chargeAttackTimer += Time.deltaTime;
            // Si el temporizador es igual o superior significa que ya se puede utilizar otra vez
            if (chargeAttackTimer >= chargeAttackCooldown) ableChargeAttack = true;
        }

        // Si el boolean est� en false se a�ade al temporizador el tiempo que pasa
        if (!ableBladestormAttack) {
            bladeStormAttackTimer += Time.deltaTime;
            // Si el temporizador es igual o superior significa que ya se puede utilizar otra vez
            if (bladeStormAttackTimer >= bladestormAttackCooldown) ableBladestormAttack = true;
        }
    }

    /// <summary>
    /// Corrutina que cambia el valor de verdadero a falso esperando un tiempo indicado por par�metro
    /// </summary>
    /// <param name="waitTime"></param>
    /// <returns></returns>
    public IEnumerator WaitForCasting(float waitTime) {
        isCasting = true;
        yield return new WaitForSeconds(waitTime);
        isCasting = false;
        canAttack = false;
        canAttackTimer = 0f;
    }

    /// <summary>
    /// Comprueba la distancia con el jugador, si es inferior o igual empezar� el combate.
    /// </summary>
    public void Engage() {
        if (Vector3.Distance(transform.position, target.position) <= engageDistance) {
            engaged = true;
            animator.SetBool("IsMoving", true);
            healthPanel.SetActive(true);
        }
    }

    /// <summary>
    /// Aplica movimiento sobre enemigo
    /// </summary>
    public void Movement() {
        ApplyRotation();
        navMeshAgent.destination = target.position;
        // Si la diferencia entre la posici�n del enemigo y el jugador es superior a la stoppingDistance puede ejecutar la animacion de caminar
        animator.SetBool("IsMoving", Vector3.Distance(transform.position, target.position) > navMeshAgent.stoppingDistance);
    }

    public void CustomMovement(float speed) {
        navMeshAgent.speed = speed;
    }

    /// <summary>
    /// Aplica la rotaci�n del enemigo con respecto al jugador
    /// </summary>
    private void ApplyRotation() {
        // Se determina la direcci�n a la que rotar
        Vector3 targetDirection = navMeshAgent.destination - transform.position;
        // Se aplica la rotaci�n de forma flu�da
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, Time.deltaTime, 0.0f);
        transform.rotation = Quaternion.LookRotation(newDirection);
    }

    /// <summary>
    /// Actualiza la barra de salud el HUD
    /// </summary>
    public void UpdateHealthBar() {
        healthBarImage.fillAmount = currentHealth / 1000;
    }

    /// <summary>
    /// M�todo de muerte del enemigo
    /// </summary>
    public void Die() {
        isDead = true;
        animator.SetTrigger("Death");
        EnemyLockOn.instance.ResetTarget();
        //Instantiate(key, transform.position, transform.rotation);
        capsuleCollider.enabled = false;

        StartCoroutine(StartTheFinalCanvas());
    }

    private IEnumerator StartTheFinalCanvas()
    {
        yield return new WaitForSeconds(3f);

        Time.timeScale = 0f;
        GameManager.instance.ChangeMenuScene(GameManager.instance.canvas[17]);

        if(Input.GetJoystickNames().Length > 0)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Confined;
        }

        DataManager.instance.DeleteSaveState();

        GameManager.instance.ChangeMenuScene(GameManager.instance.canvas[0]);

    }
    /// <summary>
    /// Funci�n que aplicar� da�o sobre el enemigo
    /// </summary>
    /// <param name="damageTaken"></param>
    public void TakeDamage(int damageTaken) {
        if (damageTaken < currentHealth) {
            currentHealth -= damageTaken;
            PlayAudioSource(damagedAudioSource, bossDamagedClip);
        } else {
            currentHealth = 0;
            PlayAudioSource(damagedAudioSource, bossDyingClip);
            Die();
        }
    }

    public void ToggleMaceStrikeCollider() {
        maceStrikeCollider.enabled = !maceStrikeCollider.enabled;
        if (maceStrikeCollider.enabled) {
            PlayAudioSource(audioSource, maceStrikeClip);
        }
    }

    public void ToggleChargeCollider() {
        chargeCollider.enabled = !chargeCollider.enabled;
        if (chargeCollider.enabled) {
            PlayAudioSource(audioSource, chargeClip);
        }
    }

    public void ToggleBladestormCollider() {
        bladestormCollider.enabled = !bladestormCollider.enabled;
        if (bladestormCollider.enabled) {
            PlayAudioSource(audioSource, bladestormClip);
        }
    }

    private void PlayAudioSource(AudioSource audSource, AudioClip clip) {
        audSource.clip = clip;
        audSource.Play();
    }
    #endregion
}
