using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class BossEffects : MonoBehaviour
{
    public bool isCharging;
    public GameObject mazeAttackVFX;
    public ParticleSystem chargeAttack;
    public GameObject dieweaponVFX;
    public GameObject dieBossVFX;
    public GameObject tornadoVFX;

    // Start is called before the first frame update
    void Start()
    {
        isCharging = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Reproduce las partículas del ataque con la maza
    /// </summary>
    public void PlayMazeAttackVFX()
    {
        mazeAttackVFX.GetComponentInChildren<VisualEffect>().Play();
        CameraShake.Instance.ShakeCamera(5, 0.5f);
    }

    /// <summary>
    /// Reproduce las partículas del ataque de la carga
    /// </summary>
    public void ChargeAttackVFXStart()
    {
        chargeAttack.Play();
    }
    
    /// <summary>
    /// Para la reproducción de las partículas del ataque de la carga
    /// </summary>
    public void ChargeAttackVFXStop()
    {
        chargeAttack.Stop();
    }

    /// <summary>
    /// Reproduce las partículas de la muerte
    /// </summary>
    public void PlayDeathWeaponVFX()
    {
        dieweaponVFX.GetComponentInChildren<VisualEffect>().Play();
    }

    /// <summary>
    /// Reproduce las partículas de la muerte
    /// </summary>
    public void PlayDeathBossVFX()
    {
        dieBossVFX.GetComponentInChildren<VisualEffect>().Play();
    }
}
