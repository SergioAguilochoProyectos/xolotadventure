using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VFX;

public abstract class Enemy : MonoBehaviour, IDamageable {
    #region Variables
    [Header("UI Settings")]
    [Tooltip("El canvas que almacena el slider de salud.")]
    public Canvas healthBarCanvas;
    [Tooltip("La barra de salud.")]
    public Slider slider;

    [Header("Enemy Stats")]
    public EnemyStats enemy;
    public float currentHealth;
    public float healingTimer;
    public float extraRotationSpeed;
    public bool isDead;

    [Header("State Controller Settings")]
    public StateController controller;

    [Header("Animations Settings")]
    public Animator animator;
    public SkinnedMeshRenderer skinnedMeshRenderer;
    public Material normalMaterial;
    public Material deadMaterial;
    public GameObject impactVFX;

    public bool isRunningCoroutine;

    public static Enemy instance;
    #endregion

    #region Unity Events

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        
    }
    private void Start() {
        currentHealth = enemy.maxHealth;
        slider.value = CalculateHealth();
    }

    private void Update() {
        slider.value = CalculateHealth();
        UpdateHealthBar();
    }
    #endregion

    #region Methods

    public abstract void Die();

    public void TakeDamage(int damageTaken) {
        controller.isDamaged = true;
        controller.lastPosition = controller.transform.position;
        currentHealth -= damageTaken;
        if (currentHealth <= 0) {
            if (!isDead) {
                Die();

                EnemyLockOn.instance.ResetTarget();

                if (MissionManager.instance != null)
                {
                    MissionManager.instance.CheckMission("mission3", 1);
                    

                    if (DataManager.instance.data.missions[2].isDone)
                    {
                        MissionManager.instance.CheckMission("mission4", 1);
                    }
                    if (DataManager.instance.data.missions[3].isDone)
                    {
                        MissionManager.instance.CheckMission("mission5", 0);
                    }
                    if (DataManager.instance.data.missions[8].isDone)
                    {
                        MissionManager.instance.CheckMission("mission10", 1);
                    }
                }
                EnemyMelee.instance.isRunningCoroutine = false;
            }
        } else {
            animator.SetTrigger("Hit");
            StartCoroutine(FlashModel(.25f));
            impactVFX.GetComponentInChildren<VisualEffect>().Play();
        }
    }

    public IEnumerator FlashModel(float waitTime) {
        skinnedMeshRenderer.material.SetColor("_BaseColor", Color.red);
        skinnedMeshRenderer.material.SetColor("EmissiveColorMap", Color.red);
        yield return new WaitForSeconds(waitTime);
        skinnedMeshRenderer.material.SetColor("_BaseColor", Color.white);
        skinnedMeshRenderer.material.SetColor("EmissiveColorMap", Color.black);
    }

    public IEnumerator Dissolve(float animDeadTimer, float waitTime) {
        skinnedMeshRenderer.material = deadMaterial;
        yield return new WaitForSeconds(animDeadTimer);
        while (skinnedMeshRenderer.material.GetFloat("_Tweak_transparency") > - 1f) {
            yield return new WaitForSeconds(waitTime);
            skinnedMeshRenderer.material.SetFloat("_Tweak_transparency", skinnedMeshRenderer.material.GetFloat("_Tweak_transparency") - .1f);
        }      
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Calcula la salud actual
    /// </summary>
    /// <returns></returns>
    public float CalculateHealth() {
        return currentHealth / enemy.maxHealth;
    }

    /// <summary>
    /// Actualiza la barra de salud.
    /// </summary>
    public void UpdateHealthBar() {
        // Mientras la salud sea inferior al m�ximo se mantiene activa la barra de salud
        if (currentHealth < enemy.maxHealth) {
            healthBarCanvas.gameObject.SetActive(true);
        }

        // Si llega la salud es igual o inferior a 0 muere el enemigo
        if (currentHealth <= 0) {
            Die();
        }

        // En caso de que la cantidad de salud supere el m�ximo de salud se equipara al m�ximo
        if (currentHealth > enemy.maxHealth) {
            currentHealth = enemy.maxHealth;
            healthBarCanvas.gameObject.SetActive(false);
        }
    }

    #endregion
}
