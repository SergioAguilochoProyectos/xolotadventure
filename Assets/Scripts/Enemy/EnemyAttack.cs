using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para poder usar el nav mesh agent
using UnityEngine.AI;

public class EnemyAttack : MonoBehaviour {

    [Tooltip("ScriptableObject que almacena la informaci�n de este enemigo.")]
    public EnemyStats enemy;

    [Tooltip("Determina si el enemigo ser� melee o ranged")]
    public bool isRanged;
    [Tooltip("Determina si el jugador est� en rango para atacar")]
    public bool playerInRange;
    [Tooltip("El transform del player")]
    public Rigidbody playerRigidbody;

    [Header("Ranged Settings")]
    public Transform firePoint;
    public EnemyBullet bulletPrefab;
    // Transform que contendr� las secciones instanciadas
    public Transform sectionContainer;

    [Header("Sounds Settings")]
    public AudioSource audioSource;
    public AudioClip meleeHitSound;
    public AudioClip bowAttackSound;

    private float timer;

    public EnemyMovement enemyMovement;
    public PlayerHealth playerHealth;

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        CheckRange();
        if (playerInRange) Attack();
        UpdateTimer();
    }

    public void CheckRange() {
        float distance = Vector3.Distance(enemyMovement.nav.transform.position, enemyMovement.nav.destination);
        if (distance < enemy.attackRange) {
            playerInRange = true;
        } else {
            playerInRange = false;
        }
    }

    public void Attack() {
        if (!playerInRange) return;

        if (isRanged) {
            RangedAttack();
        } else {
            MeleeAttack();
        }

    }

    public void MeleeAttack() {
        if (timer > enemy.attackSpeed) {
            timer = Time.deltaTime;
            playerHealth.TakeDamage(enemy.attackDamage);
            audioSource.PlayOneShot(meleeHitSound);
            playerRigidbody.AddForce((playerRigidbody.transform.position - transform.position).normalized * 200f);
        }
    }

    public void RangedAttack() {
        if (timer > enemy.attackSpeed) {
            audioSource.PlayOneShot(bowAttackSound);
            timer = Time.deltaTime;
            // Se instancia una nueva bala y se almacena como referencia de la secci�n actual
            EnemyBullet bullet = Instantiate(bulletPrefab, firePoint.position, transform.rotation, sectionContainer);
            bullet.rigidbody.AddRelativeForce(new Vector3(0f, 0f, 700f));
        }
    }

    /// <summary>
    /// Actualiza el timer del fire rating
    /// </summary>
    private void UpdateTimer() {
        timer += Time.deltaTime;
    }
}
