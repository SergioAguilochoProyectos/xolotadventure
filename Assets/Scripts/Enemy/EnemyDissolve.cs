using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDissolve : MonoBehaviour
{
    public Shader dissolveShader;

    private Renderer[] charRend;

    List<Renderer> rendererList = new List<Renderer>();

    void Start()
    {
        charRend = GetComponentsInChildren<Renderer>();
    }


    public void CharacterDie()
    {

        foreach (Renderer rend in charRend)
        {
            rendererList.Add(rend);

            rend.material.shader = dissolveShader;
        }

    }
}
