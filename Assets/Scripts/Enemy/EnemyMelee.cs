using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyMelee : Enemy {

    public int enemyId;

    public Collider weaponCollider;
    public Rigidbody enemyLizardRb;
    public Collider enemyLizardCollider;
    public Canvas hud;

    public bool isCoroutineActive = false;

    private void Start()
    {
        currentHealth = 100;

        if (DataManager.instance.data.enemyData[enemyId].isDead == true)
        {
            gameObject.SetActive(false);
        }
    }
    public override void Die() {

        DataManager.instance.CheckForEnemy(enemyId, true);

        // Se desactiva la IA
        controller.aiActive = false;
        controller.navMeshAgent.isStopped = true;
        Destroy(gameObject.GetComponent<Rigidbody>());

        Invoke("DestroyCollider", 0.2f);

        if (!isDead)
        {
            animator.SetTrigger("Death");
            isDead = true;
            DataManager.instance.data.moneyCount += 150;
        }

        if (isCoroutineActive)
        {
            StartCoroutine(DelayDisplayMessage());
        }
        // Se suma al morir el enemigo la cantidad de dinero

        StartCoroutine(Dissolve(3f, 0.5f));

        if (gameObject.CompareTag("Boss"))
        {
            GameManager.instance.WinMenu();
            hud.enabled = false;
        }

    }

    /// <summary>
    /// Activa el collider del arma
    /// </summary>
    private void ActivateCollider()
    {
        weaponCollider.enabled = true;
    }

    /// <summary>
    /// Desactiva el collider del arma
    /// </summary>
    private void DeactivateCollider()
    {
        weaponCollider.enabled = false;
    }

    /// <summary>
    /// Desactiva el collider del arma
    /// </summary>
    private void DestroyCollider()
    {
        enemyLizardCollider.enabled = false;
    }

    private IEnumerator DelayDisplayMessage()
    {
        TextManager.instance.DisplayMessage(TranslateManager.instance.GetString("tutorial4"), Color.white, "");

        PlayerController.instance.canMove = false;

        PlayerDash.instance.canDash = false;

        yield return new WaitForSeconds(4f);

        PlayerController.instance.canMove = true;

        PlayerDash.instance.canDash = true;

        TextManager.instance.HideMessage();

        isCoroutineActive = false;
    }
}
