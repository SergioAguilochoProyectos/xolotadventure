using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para poder usar el nav mesh agent
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour {

    #region Variables
    [Tooltip("Tag objetivo a buscar")]
    public string targetTagName = "Player";

    // Objetivo para desplazamiento
    private Transform target;
    [Tooltip("Almacena la referencia al navmesh agent")]
    public NavMeshAgent nav;
    public EnemyAttack enemyAttack;
    #endregion

    #region Unity Events
    // Start is called before the first frame update
    void Start() {
        // Se localiza el objetivo m?s cercano
        CheckForTarget(targetTagName);
    }

    // Update is called once per frame
    void Update() {
        // Si el navmeshagent est? activo y si existe un objetivo asignado
        if (nav.enabled && target != null) {
            // Se indica el target como destino al navmesh agent
            nav.SetDestination(target.position);
            nav.isStopped = enemyAttack.playerInRange;
            FaceTarget();
        }
    }
    #endregion

    #region Methods
    /// <summary>
    /// Busca el objetivo m?s cercano con el tag indicado
    /// </summary>
    /// <param name="name"></param>
    public void CheckForTarget(string name) {
        // Busca entre todos los objetos de la escena aquellos que coincidan con el tag indicado
        GameObject[] possibleTargets = GameObject.FindGameObjectsWithTag(name);
        // Se recorre todos los objetos encontrados
        foreach (GameObject possibleTarget in possibleTargets) {
            // Si no hay objetivo
            if (target == null) {
                // Se fija el primero listado
                target = possibleTarget.transform;
            }
            // Si la distancia entre el target actual es mayor a la distancia entre el posible target
            else if (Vector3.Distance(transform.position, target.position) > Vector3.Distance(transform.position, possibleTarget.transform.position)) {
                // Se actualiza el target actual con la informaci?n del posible target al estar ?ste m?s cerca
                target = possibleTarget.transform;
            }
        }
    }

    public void FaceTarget() {
        Vector3 lookPosition = target.position - transform.position;
        lookPosition.y = 0f;
        transform.rotation = Quaternion.Slerp(transform.rotation,
                                            Quaternion.LookRotation(lookPosition),
                                            1f);
    }
    #endregion
}
