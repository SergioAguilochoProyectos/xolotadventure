using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRanged : Enemy {
    public int enemyId;

    public Rigidbody enemyLizardRb;
    public Collider enemyLizardCollider;
    public GameObject arrowPrefab;
    private bool arrowCreated;

    private void Start()
    {
        currentHealth = 100;


        if (DataManager.instance.data.enemyData[enemyId].isDead == true)
        {
            gameObject.SetActive(false);
        }

    }
    public override void Die() {

        DataManager.instance.CheckForEnemy(enemyId, true);
        
        // Se desactiva la IA
        controller.aiActive = false;
        controller.navMeshAgent.isStopped = true;
        enemyLizardCollider.enabled = false;
        if (!arrowCreated) {
            Instantiate(arrowPrefab, transform.position, arrowPrefab.transform.rotation);
            arrowCreated = true;
        }

        // Se suma al morir el enemigo la cantidad de dinero

        Destroy(gameObject.GetComponent<Rigidbody>());

        if (!isDead)
        {
            animator.SetTrigger("Death");
            isDead = true;
            DataManager.instance.data.moneyCount += 100;
        }

        StartCoroutine(Dissolve(3f, 0.5f));
    }
}
