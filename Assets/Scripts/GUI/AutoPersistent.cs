using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class AutoPersistent : MonoBehaviour
{
#if UNITY_EDITOR // Si te encuentras en el EDITOR lo compilar�, SOLO. Puedes elegir otra Conditional Compilation
    //No ir� en la BUILD este apartado
    private void Awake()
    {
        if (SceneManager.GetSceneByBuildIndex(0).name != "Persistent")
        {
            SceneManager.LoadScene("Persistent");
        }
    }

#endif
}
