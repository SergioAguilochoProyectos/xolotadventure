using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

public enum BlindStates { Normal, Achromatomaly, Achromatopsia, Deuteranomaly, Deuteranopia, Protanomaly, Protanopia, Tritanomaly, Tritanopia }
public class Colorblind_Selector : MonoBehaviour
{

    public Volume m_volume;

    [Header("Colorblind Fix")]
    public BlindStates colorblindState;

    public static Colorblind_Selector instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }

    }

    void Update()
    {
        ChangeStates(colorblindState);
    }

    public void ChangeStates(BlindStates state)
    {
        VolumeProfile profile = m_volume.sharedProfile;

        if (!profile.TryGet<ChannelMixer>(out var channel))
        {
            channel = profile.Add<ChannelMixer>(false);
        }

        if (state == BlindStates.Normal)
        {
            channel.redOutRedIn.Override(100f);
            channel.redOutGreenIn.Override(0f);
            channel.redOutBlueIn.Override(0f);

            channel.greenOutRedIn.Override(0f);
            channel.greenOutGreenIn.Override(100f);
            channel.greenOutBlueIn.Override(0f);

            channel.blueOutRedIn.Override(0f);
            channel.blueOutGreenIn.Override(0f);
            channel.blueOutBlueIn.Override(100f);
        }

        else if (state == BlindStates.Achromatomaly)
        {
            channel.redOutRedIn.Override(61.8f);
            channel.redOutGreenIn.Override(32f);
            channel.redOutBlueIn.Override(6.2f);

            channel.greenOutRedIn.Override(16.3f);
            channel.greenOutGreenIn.Override(77.5f);
            channel.greenOutBlueIn.Override(6.2f);

            channel.blueOutRedIn.Override(16.3f);
            channel.blueOutGreenIn.Override(32f);
            channel.blueOutBlueIn.Override(51.6f);
        }

        else if (state == BlindStates.Achromatopsia)
        {
            channel.redOutRedIn.Override(29.9f);
            channel.redOutGreenIn.Override(58.7f);
            channel.redOutBlueIn.Override(11.4f);

            channel.greenOutRedIn.Override(29.9f);
            channel.greenOutGreenIn.Override(58.7f);
            channel.greenOutBlueIn.Override(11.4f);

            channel.blueOutRedIn.Override(29.9f);
            channel.blueOutGreenIn.Override(58.7f);
            channel.blueOutBlueIn.Override(11.4f);

        }

        else if (state == BlindStates.Deuteranomaly)
        {
            channel.redOutRedIn.Override(80f);
            channel.redOutGreenIn.Override(20f);
            channel.redOutBlueIn.Override(0f);

            channel.greenOutRedIn.Override(25.833f);
            channel.greenOutGreenIn.Override(74.167f);
            channel.greenOutBlueIn.Override(0f);

            channel.blueOutRedIn.Override(0f);
            channel.blueOutGreenIn.Override(14.167f);
            channel.blueOutBlueIn.Override(85.833f);
        }
        else if (state == BlindStates.Deuteranopia)
        {
            channel.redOutRedIn.Override(62.5f);
            channel.redOutGreenIn.Override(37.5f);
            channel.redOutBlueIn.Override(0f);

            channel.greenOutRedIn.Override(70f);
            channel.greenOutGreenIn.Override(30f);
            channel.greenOutBlueIn.Override(0f);

            channel.blueOutRedIn.Override(0f);
            channel.blueOutGreenIn.Override(30f);
            channel.blueOutBlueIn.Override(70f);
        }
        else if (state == BlindStates.Protanomaly)
        {
            channel.redOutRedIn.Override(81.667f);
            channel.redOutGreenIn.Override(18.333f);
            channel.redOutBlueIn.Override(0f);

            channel.greenOutRedIn.Override(33.333f);
            channel.greenOutGreenIn.Override(66.667f);
            channel.greenOutBlueIn.Override(0f);

            channel.blueOutRedIn.Override(0f);
            channel.blueOutGreenIn.Override(12.5f);
            channel.blueOutBlueIn.Override(87.5f);
        }
        else if (state == BlindStates.Protanopia)
        {
            channel.redOutRedIn.Override(56.667f);
            channel.redOutGreenIn.Override(43.333f);
            channel.redOutBlueIn.Override(0f);

            channel.greenOutRedIn.Override(55.833f);
            channel.greenOutGreenIn.Override(44.167f);
            channel.greenOutBlueIn.Override(0f);

            channel.blueOutRedIn.Override(0f);
            channel.blueOutGreenIn.Override(24.167f);
            channel.blueOutBlueIn.Override(75.833f);

        }
        else if (state == BlindStates.Tritanomaly)
        {
            channel.redOutRedIn.Override(96.667f);
            channel.redOutGreenIn.Override(3.333f);
            channel.redOutBlueIn.Override(0f);

            channel.greenOutRedIn.Override(0f);
            channel.greenOutGreenIn.Override(73.333f);
            channel.greenOutBlueIn.Override(26.667f);

            channel.blueOutRedIn.Override(0f);
            channel.blueOutGreenIn.Override(18.333f);
            channel.blueOutBlueIn.Override(81.667f);
        }
        else if (state == BlindStates.Tritanopia)
        {
            channel.redOutRedIn.Override(95f);
            channel.redOutGreenIn.Override(5f);
            channel.redOutBlueIn.Override(0f);

            channel.greenOutRedIn.Override(0f);
            channel.greenOutGreenIn.Override(43.333f);
            channel.greenOutBlueIn.Override(56.667f);

            channel.blueOutRedIn.Override(0f);
            channel.blueOutGreenIn.Override(47.5f);
            channel.blueOutBlueIn.Override(52.5f);
        }
    }
}
