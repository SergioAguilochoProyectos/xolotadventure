using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ExecuteScene : MonoBehaviour
{

    public string sceneName;

    [Header("Unlock Travelling")]
    public Button button;
    public int missionNumber;

    public static ExecuteScene instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }

    }
    private void Update()
    {
        if (button.name == "Play")
        {
            // De la misi�n 1 a 5 viaje a DarkForest
            if (DataManager.instance.data.missionCounter >= 0 && DataManager.instance.data.missionCounter < 6)
            {
                sceneName = "DarkForest";
            }
            // De la misi�n 6 a 8 o de la misi�n 12 a 13 viaje a GalapuaVillage
            else if (DataManager.instance.data.missionCounter >= 6 && DataManager.instance.data.missionCounter < 9 ||
                DataManager.instance.data.missionCounter >= 12 && DataManager.instance.data.missionCounter < 13)
            {
                sceneName = "GalapuaVillage";
            }
            // De la misi�n 9 a 11 viaje a FireCamp 1
            else if (DataManager.instance.data.missionCounter >= 9 && DataManager.instance.data.missionCounter < 12)
            {
                sceneName = "FireCamp 1";
            }
            // De la misi�n 12 a 13 viaje a LohokDungeon
            else if (DataManager.instance.data.missionCounter == 13)
            {
                sceneName = "Dungeon";
            }
            else
            {
                sceneName = DataManager.instance.data.currentScene;
            }
        }
    }
    /// <summary>
    /// Carga la escena usando el SceneController con el Fade
    /// </summary>
    public void SceneLoadExecute()
    {
        Time.timeScale = 1f;

        string scene = DataManager.instance.data.currentScene;

        string sceneToTravel = sceneName;

        if (sceneName == sceneToTravel && DataManager.instance.data.missions[missionNumber].isDone || sceneName == "DarkForest" || sceneName == "MainMenu")
        {
            SceneController.instance.FadeAndLoadScene(sceneName, DataManager.instance.data.entrancePosition);
        }
       
    }

}
