using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GamepadSelection : MonoBehaviour, ISelectHandler, IDeselectHandler
{
    private Button button;
    public AudioSource audioSource;
    public AudioClip buttonAudio;
    public GameObject selection;

    private Animator buttonAnim;
    void Start()
    {
        button = GetComponent<Button>();
        buttonAnim = GetComponent<Animator>();

        if(button.name == "Play" && Input.GetJoystickNames().Length > 0)
        {
            buttonAnim.SetBool("Selected", true);
            if (selection != null)
            {
                selection.SetActive(true);
            }

        }

    }
    public void OnSelect(BaseEventData eventData)
    {
        if(buttonAnim != null)
        {
            buttonAnim.SetBool("Selected", true);
        }

        audioSource.PlayOneShot(buttonAudio);

        if (selection != null)
        {
            selection.SetActive(true);
        }
    }
    
    public void OnDeselect(BaseEventData eventData)
    {
        if(buttonAnim != null)
        {
            buttonAnim.SetBool("Selected", false);
        }

        if (selection != null)
        {
            selection.SetActive(true);
        }
    }
}
