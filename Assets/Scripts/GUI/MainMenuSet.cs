using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuSet : MonoBehaviour
{

    public GameObject mainMenuCanvas;
    public GameObject backgroundBook;
    public GameObject pressAnyButton;

    public void MainMenuActive()
    {
        mainMenuCanvas.SetActive(true);
        backgroundBook.SetActive(true);
        gameObject.SetActive(false);
    }

    public void PressAnyButton()
    {
        pressAnyButton.SetActive(false);
    }
}
