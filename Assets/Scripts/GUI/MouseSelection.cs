using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MouseSelection : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private Button button;
    public AudioSource audioSource;
    public AudioClip buttonAudio;
    public GameObject selectionL, selectionR;
    void Start()
    {
        button = GetComponent<Button>();

        if(button.name == "Play" && Input.GetJoystickNames().Length > 0)
        {
            button.GetComponentInChildren<Text>().color = Color.yellow;
            if(selectionL != null || selectionR != null)
            {
                selectionL.SetActive(true);
                selectionR.SetActive(true);
            }

        }

    }
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (button.name != "Tutorial")
        {
            button.GetComponentInChildren<Text>().color = Color.yellow;
        }
        audioSource.PlayOneShot(buttonAudio);
        if (selectionL != null || selectionR != null)
        {
            selectionL.SetActive(true);
            selectionR.SetActive(true);
        }
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        if(button.name != "Tutorial")
        {
            button.GetComponentInChildren<Text>().color = Color.white;
        }
        if (selectionL != null || selectionR != null)
        {
            selectionL.SetActive(false);
            selectionR.SetActive(false);
        }
    }

}
