using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    public CanvasGroup pauseMenuCanvas;


    private void Update()
    {
        Pause();
        
    }

    public void Pause()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 0;

            Cursor.visible = true;

            Utils.ActiveCanvasGroup(pauseMenuCanvas, true);
        }
    }

    /// <summary>
    /// Despausa la partida
    /// </summary>
    public void Resume()
    {
        Time.timeScale = 1;

        Utils.ActiveCanvasGroup(pauseMenuCanvas, false);
    }
}
