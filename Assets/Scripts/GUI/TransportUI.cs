using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TransportUI : MonoBehaviour
{
    public GameObject buttonToPress;
    public TextMesh textFromButton;
    public string textToShow;
    public Animator animButton;

    public static TransportUI instance;

    public bool canUseMap;

    private SphereCollider sCollider;
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }


    private void Start()
    {
        buttonToPress.SetActive(false);
        sCollider = GetComponent<SphereCollider>();
    }

    private void Update()
    {
        if (canUseMap)
        {
            if (Input.GetButtonDown("Interact"))
            {
                Interaction();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //canUseMap = true;

            if (Input.GetJoystickNames().Length > 0)
            {
                EventSystem.current.SetSelectedGameObject(GameManager.instance.firstButtonFromMenu[9]);
            }
        }
    }

    public void OnTriggerStay(Collider collision)
    {
        if (GameManager.instance.canvas[4].alpha == 0)
        {
            ShowMessage();
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            canUseMap = true;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            canUseMap = false;
        }
    }

    public void Interaction()
    {
        if(GameManager.instance.canvas[4].alpha == 0 && canUseMap == true)
        {
            GameManager.instance.ChangeMenuScene(GameManager.instance.canvas[4]);
            //Utils.ActiveCanvasGroup(GameManager.instance.canvas[4], true);
            Debug.Log("He entrado en Interaction");
        }

            HideMessage();

            PlayerController.instance.canMove = false;

            if (Input.GetJoystickNames().Length > 0)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }

            GameManager.instance.cameraFollow.enabled = false;
        
    }

    public IEnumerator DelayOnTrigger()
    {
        sCollider.enabled = false;
        //canUseMap = false;

        yield return new WaitForSeconds(3f);

        //canUseMap = true;
        sCollider.enabled = true;
    }

    private void OnTriggerExit()
    {
        HideMessage();
    }

    private void ShowMessage()
    {
        buttonToPress.SetActive(true);
        animButton.SetBool("Press", true);

        textFromButton.text = textToShow;
    }

    private void HideMessage()
    {
        buttonToPress.SetActive(false);
        animButton.SetBool("Press", false);
    }

}
