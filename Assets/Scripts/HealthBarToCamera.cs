using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarToCamera : MonoBehaviour {
    #region Variables
    public Camera mainCamera;
    public Slider healthBar;
    #endregion

    #region Unity Events
    // Update is called once per frame
    void Update() {
        RotateHealthBar();
    }
    #endregion

    #region Methods
    public void RotateHealthBar() {
        healthBar.transform.LookAt(healthBar.transform.position + mainCamera.transform.rotation * Vector3.forward,
                        mainCamera.transform.rotation * Vector3.up);
    }
    #endregion

}
