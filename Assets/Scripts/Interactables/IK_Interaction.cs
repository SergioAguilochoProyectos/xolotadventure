using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IK_Interaction : MonoBehaviour
{
    #region VARIABLES
    public Animator animator;

    public float movementDuration = 1f; //Duraci�n del movimiento de interacci�n
    private float movementTimer; //Contador de tiempo de interacci�n
    private float interpolation; //Interpolaci�n en curva de animaci�n

    public AnimationCurve curve; //Curva de animaci�n para definir el IK Weight
    public Transform target = null; //Objetivo del IK
    public AvatarIKGoal ikGoal; //IKGoal del avatar, 4 extremidades disponibles (brazos y piernas)
    #endregion
    #region UNITY EVENTS
    private void Update()
    {
        if (movementTimer > 0)
        {
            movementTimer -= Time.deltaTime;
            interpolation = 1 - movementTimer / movementDuration; //Se eval�a la curva en funci�n del tiempo de animaci�n transcurrido
        }
    }
    private void OnAnimatorIK(int layerIndex)
    {
        if (!animator || !target) return; //Si no hay definido Animator o Target, no se hace nada

        //IK_Weight de la cabeza del Player mirando al objetivo en funci�n del peso de la animaci�n
        animator.SetLookAtWeight(curve.Evaluate(interpolation));
        animator.SetLookAtPosition(target.position); //Se indica la direcci�n a la que mirar� la cabeza del player

        animator.SetIKPositionWeight(ikGoal, curve.Evaluate(interpolation)); //Definimos Weight de la posici�n para el movimiento de la IK
        animator.SetIKPosition(ikGoal, target.position);

        animator.SetIKRotationWeight(ikGoal, curve.Evaluate(interpolation)); //Definimos Weight de la rotaci�n para el movimiento de la IK
        animator.SetIKRotation(ikGoal, target.rotation);
    }
    #endregion
    #region METHODS
    /// <summary>
    /// Inicia el movimiento de la IK
    /// </summary>
    [ContextMenu("Start Movement")]
    public void StartMovement()
    {
        movementTimer = movementDuration;
    }
    #endregion
}
