using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    #region VARIABLES
    // Transform que indicar� la posici�n y rotaci�n a la que el jugador mirar� al interactuar
    public Transform interactionLocation;
    // Array de condiciones que se deben cumplir para acceder a las reacciones positivas
    [Header("Conditions")]
    public string[] conditions;
    // Bloquea al jugador para que no se pueda mover durante la interacci�n
    public bool lockPlayer = true;

    // Jugador que est� realizando la interacci�n
    private PlayerController interactingPlayer;

    // Parent de las reacciones positivas
    private Transform positiveReactions;
    // Parent de las reacciones por defecto
    private Transform defaultReactions;

    // Cola para gestionar las reacciones a realizar
    private Queue<Reaction> reactionQueue = new Queue<Reaction>();

    public bool isPlayingReaction = false;
    #endregion
    #region UNITY EVENTS

    protected virtual void Start()
    {
        positiveReactions = transform.Find("PositiveReactions");

        defaultReactions = transform.Find("DefaultReactions");

    }

    private void OnDisable()
    {
        if (interactingPlayer)
        {
            interactingPlayer.canMove = true;
            interactingPlayer = null;
        }

    }
    #endregion
    #region METHODS
    /// <summary>
    /// Gestionar� las condiciones y las reacciones de cada interacci�n
    /// </summary>
    public void Interact(PlayerController player)
    {
        isPlayingReaction = true;
        // Se dan por cumplidas las condiciones
        bool success = true;
        // Asignamos el player que estar� interactuando
        interactingPlayer = player;

        // Se recorren todas las condiciones del interactable
        foreach (string condition in conditions)
        {
            if (!DataManager.instance.CheckCondition(condition))
            {
                success = false;
                break; // Si no se cumple, rompemos el bucle
            }
        }
        // Si se cumplen las condiciones y el n�mero es mayor que 0
        if (success && conditions.Length > 0)
        {
            // Se recupera la lista de reacciones positivas
            QueueReactions(positiveReactions);
        }
        else
        {
            // En caso contrario, se recupera la lista de reacciones por defect
            QueueReactions(defaultReactions);
        }

        // Una vez preparada la cola, ejecutamos la primera reacci�n
        NextReaction();
    }
    /// <summary>
    /// Pone en cola todas las reacciones que contenga el transform recibido como par�metro
    /// </summary>
    /// <param name="reactionContainer"></param>
    public void QueueReactions(Transform reactionContainer)
    {
        reactionQueue.Clear();

        foreach (Reaction reaction in reactionContainer.GetComponentsInChildren<Reaction>())
        {
            reaction.interactable = this;
            reactionQueue.Enqueue(reaction);
        }
    }
    /// <summary>
    /// Iniciar la siguiente reacci�n de la cola
    /// </summary>
    public void NextReaction()
    {
        if (reactionQueue.Count > 0)
        {
            // Si el interactable ha solicitado bloquear al jugador, se impide el movimiento
            interactingPlayer.canMove = !lockPlayer;

            // Recupera el siguiente elemento y lo ejecuta
            reactionQueue.Dequeue().ExecuteReaction();
        }
        else
        {
            // Si no quedan reacciones, liberamos al jugador
            //interactingPlayer.canMove = true;
            // Y eliminamos su referencia que quede

            interactingPlayer.interactable = this;

            interactingPlayer = null;

            PlayerHealth.instance.canHeal = true;

            isPlayingReaction = false;
        }
    }

    #endregion
}
