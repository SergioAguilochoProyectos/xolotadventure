using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para hacer b�squedas en Array
using System.Linq;
public class InteractableItem : Interactable
{
    // Nombre �nico del item, para localizarlo en el listado allItems
    public string itemName;

    protected override void Start()
    {
        base.Start();

        // Se desactiva el objeto si ya ha sido recogido
        if (IsPicked())
        {
            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Verifica si el objeto ya ha sido recogido
    /// </summary>
    /// <returns></returns>
    private bool IsPicked()
    {
        // Se recupera el item por su nombre
        BinaryItem result = DataManager.instance.data.allItems.Where(i => i.name == itemName).FirstOrDefault();
        // En caso de recuperar un item
        if (result != null)
        {
            // Se devuelve su estado de "picked" = TRUE
           return result.picked;
        }
        Debug.LogWarning("El nombre no existe en la lista de items: " + itemName);
        return false;
    }

}
