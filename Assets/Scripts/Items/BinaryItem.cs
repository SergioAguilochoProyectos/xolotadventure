[System.Serializable]
public class BinaryItem
{
    #region Variables
    public string name;
    // Descripci�n para nosotros, los programadores
    public string description;
    // Nombre de la imagen para recuperarla de Resources
    public string imageName;
    // Boolean que identifica si el item es �nico
    public bool unique;
    // Cantidad de items
    public int amount;
    // Indicar si se ha recogido el objeto o no
    public bool picked;
    #endregion
}
