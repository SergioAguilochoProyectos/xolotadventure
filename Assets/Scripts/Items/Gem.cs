using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Gem : Item, IPickable, IStorable {
    #region Variables
    public AudioSource audioSource;
    public Animator animator;
    #endregion

    #region Unity Events
    private void Awake() {
        // Las gemas al tratarse de objetos �nicos se inicializan como true
        unique = true;
    }

    private void Start() {
        audioSource = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
        // Si el objeto est� recogido
        if (DataManager.instance.GetItem(itemName).picked && DataManager.instance.GetItem(itemName).unique) {
            // Se desactiva
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other) {
        // Si el jugador entra en contacto con el item
        if (other.tag.ToString() == "Player") {
            // Lo recoge
            if (DataManager.instance.GetItem("Jade").picked == false)
            {
                PickUp();
                MissionManager.instance.CheckMission("mission5", 1);

                TextManager.instance.DisplayMessage(TranslateManager.instance.GetString("tutorial6"), Color.black, "");

                MissionManager.instance.CheckMission("mission6", 0);

                //InventoryManager.instance.ChangeInventorySlotImage();
            }
            if (DataManager.instance.data.missions[8].isDone && DataManager.instance.GetItem("Infernal").picked == false)
            {
                PickUp();

                TextManager.instance.DisplayMessage(TranslateManager.instance.GetString("finishGame"), Color.black, "");

                //InventoryManager.instance.ChangeInventorySlotImage();
            }

        }
    }
    #endregion

    #region Methods

    
    private IEnumerator DelayFinishGame()
    {
        //TextManager.instance.DisplayMessage(TranslateManager.instance.GetString("finishGame"), Color.black);

        yield return new WaitForSeconds(2f);

        TextManager.instance.HideMessage();

        DataManager.instance.DeleteSaveState();

        SceneController.instance.FadeAndLoadScene("MainMenu", "");
    }


    public void PickUp() {
        // Se asigna el valor a recogido
        picked = true;
        animator.SetTrigger("Collect");
        GetComponent<BoxCollider>().enabled = false;
        // Si no est� en el inventario
        if (!IsStored(name.Split('_')[0])) {
            // Lo almacena
            StoreItem();
        }

        StartCoroutine(Banish());
    }
    
    public void StoreItem() {
        picked = true;
        //InventoryManager.instance.SaveItemOnInventory(InventoryManager.instance.jadeGem);
        //InventoryManager.instance.SaveItemOnInventory(InventoryManager.instance.fireGem);
        //InventoryManager.instance.AddItemInventory("Jade");

        if (DataManager.instance.data.missions[8].isDone)
        {
            InventoryManager.instance.AddItemInventory("Infernal");
        }
        DataManager.instance.SetItem(itemName, true);
        DataManager.instance.Save();
    }

    public bool IsStored(string itemName) {
        return DataManager.instance.CheckPickedItem(itemName);
    }

    public IEnumerator Banish() {
        // Se reproduce el efecto de sonido
        audioSource.Play();
        // Se espera por la duraci�n del efecto de sonido
        yield return new WaitForSeconds(audioSource.clip.length);
        // Se desactiva
        gameObject.SetActive(false);
    }
    #endregion
}
