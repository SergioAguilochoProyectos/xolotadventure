using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEquippable
{
    /// <summary>
    /// Instrucción para equipar un item
    /// </summary>
    void Equip();

    /// <summary>
    /// Instrucción para desequipar un item
    /// </summary>
    void Unequip();
}
