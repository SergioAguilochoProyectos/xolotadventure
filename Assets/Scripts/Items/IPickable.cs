using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPickable
{
    /// <summary>
    /// M�todo que definir� la l�gica para recoger el item
    /// </summary>
    void PickUp();
}
