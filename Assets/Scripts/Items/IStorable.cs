using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStorable
{
    /// <summary>
    /// M�todo que definir� la l�gica para almacenar el item
    /// </summary>
    void StoreItem();

    bool IsStored(string itemName);
}
