using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockTarget : MonoBehaviour
{
    public float distanceMax;
    private GameObject enemy;
    public Camera mainCamera;
    public Collider sphereCollider;
    void Start()
    {
        enemy = GameObject.FindGameObjectWithTag("Enemy");
    }

    void Update()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        float distance = Vector3.Distance(transform.position, enemy.transform.position);

        if (other.CompareTag("Enemy") && distance < distanceMax)
        {
            mainCamera.transform.rotation = enemy.transform.rotation;
        }
    }
}
