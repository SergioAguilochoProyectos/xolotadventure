using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class AchievementManager : MonoBehaviour
{
    //Tienen que tener 2 strings para PODER suscribirse a este Action
    public static Action<string, string> OnAchievementUnlock;

    public static AchievementManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void OnEnable()
    {

    }

    /// <summary>
    /// Incrementamos una estadística, y se verifican sus logros asociados
    /// </summary>
    /// <param name="code"></param>
    /// <param name="amount"></param>
    public void IncreaseStatAndCheckAchievement(string code, int amount)
    {
        //Se realiza una CONSULTA como en SQL, mediante el uso de "linq"
        //Haciendo una consulta para recuperar todo aquello que coincida
        Stat stat = DataManager.instance.data.statistics.Where(s => s.code == code).FirstOrDefault();

        if (stat == null) return;

        stat.value += amount;

        foreach (Achievement achievement
            in DataManager.instance.data.achievements.Where(a => a.statCode == code && !a.targetUnlocked).AsEnumerable())
        {
            if (stat.value >= achievement.targetAmount)
            {
                achievement.targetUnlocked = true;

                OnAchievementUnlock?.Invoke(achievement.name, achievement.imageName);

                IncreaseStatAndCheckAchievement("allAchievements", 1);

                DataManager.instance.Save();
            }
        }
    }
}
