using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CheatsManager : MonoBehaviour
{
    public PlayerHealth health;
    public PlayerStamina stamina;
    public PlayerAttack attack;
    public PlayerDash dash;
    public PlayerController controller;
    public EnemyLockOn lockOn;
    public CameraFollow cameraFollow;

    public Toggle[] cheatButtons;
    public bool isFullDmg;

    public SceneController sceneController;

    public static CheatsManager instance;

    private void Awake()
    {
        if(instance == null) 
        { 
            instance = this; 
        }
    }
    private void Update()
    {
        Controls();

        if (cheatButtons[0].isOn)
        {
            InfiniteHealth();
        }
        if (cheatButtons[1].isOn)
        {
            InfiniteStamina();
        }
        if (cheatButtons[2].isOn)
        {
            MaxSpeed();
        }
        else
        {
            if(controller != null)
            {
                controller.walkSpeed = 5;
                controller.runSpeed = 10;
            }
        }
        if (cheatButtons[4].isOn)
        {
            isFullDmg = true;
            FullDmg();
        }
        else
        {
            if (attack != null)
            {
                isFullDmg = false;
                attack.currentAttackDamage = attack.baseAttackDamage;
            }
        }
        if (cheatButtons[3].isOn)
        {
            NoClip(true);
        }
        else
        {
            NoClip(false);
        }
        if (GameManager.instance.cheatMenuCanvas.alpha == 1)
        {
            controller.canMove = false;

            attack.canAttack = false;

            dash.canDash = false;

            if (lockOn != null)
            {
                lockOn.enabled = false;
            }

        }
        else
        {
            if(attack != null)
            {
                attack.canAttack = true;
            }
            if(lockOn != null)
            {
                lockOn.enabled = true;
            }

        }
    }

    public void Controls()
    {
        if (Input.GetButtonDown("Cheats"))
        {
            Debug.Log("Cheats");
            OpenAndCloseCheatsMenu();
        }
    }
    public void OpenAndCloseCheatsMenu()
    {
        bool isToggled = GameManager.instance.cheatMenuCanvas.alpha == 0 ? false : true;

        Utils.ActiveCanvasGroup(GameManager.instance.cheatMenuCanvas, !isToggled);

        if (!isToggled)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

            controller.anim.SetFloat("Forward", 0f);
            controller.anim.SetFloat("Right", 0f);

            if (lockOn != null)
            {
                lockOn.enabled = false;
            }
        }
        else
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;

            controller.canMove = true;

            dash.canDash = true;

            if (lockOn != null)
            {
                lockOn.enabled = true;
            }
        }
    }
    /// <summary>
    /// Proporciona vida constante al player que no se reduce 
    /// </summary>
    public void InfiniteHealth()
    {
        DataManager.instance.data.playerHealth = health.maxHealth;
    }
    /// <summary>
    /// Proporciona estamina constante al player que no se reduce 
    /// </summary>
    public void InfiniteStamina()
    {
        stamina.currentStamina = stamina.maxStamina;

        stamina.staminaBar.fillAmount = stamina.maxStamina;
    }
    public void MaxSpeed()
    {
        controller.walkSpeed = 25f;
        controller.runSpeed = 25f;
    }
    public void NoClip(bool activate)
    {
        // Entre layer "Player" y "Wall" se IGNORAN
        Physics.IgnoreLayerCollision(3, 8, activate);
    }
    public void FullDmg()
    {
        attack.currentAttackDamage = attack.baseAttackDamage * 10;
    }
    public void ChangeScene(string name)
    {
        sceneController.FadeAndLoadScene(name, "");
        Utils.ActiveCanvasGroup(GameManager.instance.cheatMenuCanvas, false);
        GameManager.instance.playerControl.canMove = true;
        GameManager.instance.playerDash.canDash = true;
        GameManager.instance.enemyLockOn.canLock = true;
    }
}
