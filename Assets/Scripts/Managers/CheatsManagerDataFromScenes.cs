using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatsManagerDataFromScenes : MonoBehaviour
{
    public PlayerHealth health;
    public PlayerStamina stamina;
    public PlayerAttack attack;
    public PlayerDash dash;
    public PlayerController controller;
    public EnemyLockOn lockOn;
    public CameraFollow cameraFollow;

    // Start is called before the first frame update
    void Start()
    {
        CheatsManager.instance.controller = controller;
        CheatsManager.instance.attack = attack;
        CheatsManager.instance.dash = dash;
        CheatsManager.instance.stamina = stamina;
        CheatsManager.instance.health = health;
        CheatsManager.instance.lockOn = lockOn;
        CheatsManager.instance.cameraFollow = cameraFollow;
    }

}
