using UnityEngine;
//Se utiliza para poder serializar en BINARIO la información
using System.Runtime.Serialization.Formatters.Binary;
//Para poder leer y grabar ficheros, IO(Input, Output)
using System.IO;
// Para poder filtrar los arrays
using System.Linq;

public class DataManager : MonoBehaviour {
    #region VARIABLES
    public Data data;
    public string filename = "data.dat";
    private string dataPath;

    public static DataManager instance;
    #endregion
    #region UNITY EVENTS

    private void Awake() {
        if (instance == null) { instance = this; }

        // Identificar rapidamente el DataPath
        Debug.Log(Application.persistentDataPath);
        // Configuración del DataPath para el guardado
        dataPath = Application.persistentDataPath + "/" + filename;

        Load();
    }
    #endregion
    #region METHODS
    [ContextMenu("SaveData")]
    /// <summary>
    /// Guarda la información en el disco
    /// </summary>
    public void Save() {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(dataPath);
        bf.Serialize(file, data);
        file.Close();
    }
    /// <summary>
    /// Recuperar la información guardada en el disco
    /// </summary>
    public void Load() {
        if (!File.Exists(dataPath)) { return; }

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(dataPath, FileMode.Open);
        data = (Data)bf.Deserialize(file);
        file.Close();
    }

    /// <summary>
    /// Devuelve el estado en que se encuentra la condición recibida como parámetro
    /// </summary>
    /// <param name="conditionName"></param>
    /// <returns></returns>
    public bool CheckCondition(string conditionName) {
        // Recuperan aquellos "c" que su nombre coincida con el conditionName
        Conditions result = data.allConditions.Where(c => c.name == conditionName).FirstOrDefault();

        if (result != null) {
            return result.done;
        }

        Debug.Log(conditionName + " La condición no existe y no se puede revisar ");

        return false;
    }
    /// <summary>
    /// Cambia el estado de una condición, al estado indicado
    /// </summary>
    /// <param name="conditionName"></param>
    /// <param name="done"></param>
    public void SetCondition(string conditionName, bool done) {
        //Se busca una condición que cumpla este criterio
        Conditions result = data.allConditions.Where(c => c.name == conditionName).FirstOrDefault();

        if (result != null) {
            // Se modifica su estado asignando el indicado
            result.done = done;
        }
    }
    /// <summary>
    /// Borra el fichero de guardado
    /// </summary>
    [ContextMenu("DeleteData")]
    public void DeleteSaveState() {
        if (File.Exists(dataPath)) {
            File.Delete(dataPath);
        }
    }


    public Item ParseBinaryItem(BinaryItem binaryItem) {
        Item item = new Item();
        item.itemName = binaryItem.name;
        item.description = binaryItem.description;
        item.imageName = binaryItem.imageName;
        item.unique = binaryItem.unique;
        item.picked = binaryItem.picked;
        return item;
    }

    public BinaryItem ParseItem(Item item) {
        BinaryItem binaryItem = new BinaryItem();
        binaryItem.name = item.itemName;
        binaryItem.description = item.description;
        binaryItem.imageName = item.imageName;
        binaryItem.unique = item.unique;
        binaryItem.picked = item.picked;
        return binaryItem;
    }

    public bool CheckPickedItem(string itemName) {
        BinaryItem result = data.allItems.Where(i => i.name == itemName).FirstOrDefault();
        if (result != null) {
            return result.picked;
        }

        Debug.Log(itemName + " El item no existe y no se puede revisar.");

        return false;
    }

    public int GetItemCount(string itemName) {
        BinaryItem result = data.allItems.Where(i => i.name == itemName).FirstOrDefault();
        if (result != null) {
            return result.amount;
        }

        Debug.Log(itemName + " El item no existe y no se puede revisar.");

        return 0;
    }

    public void SetItem(string itemName, bool picked) {
        Debug.Log(itemName + ", " + picked);
        BinaryItem result = data.allItems.Where(i => i.name == itemName).FirstOrDefault();
        if (result != null) {
            // Se modifica su estado asignando el indicado
            result.picked = picked;
        }
    }

    public Item GetItem(string itemName) {
        BinaryItem result = data.allItems.Where(i => i.name == itemName).FirstOrDefault();
        if (result != null) {
            return ParseBinaryItem(result);
        }

        Debug.Log(itemName + " El item no existe y no se puede revisar.");

        return null;
    }

    public void CheckForEnemy(int id, bool isDead)
    {
        for (int i = 0; i < data.enemyData.Length; i++)
        {
            if (data.enemyData[id].enemyId == id)
            {
                data.enemyData[id].isDead = isDead;
            }

        }

    }
    #endregion
}

