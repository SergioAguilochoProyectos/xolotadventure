using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour {
    #region Variables
    [Header("MainMenu")]
    public GameObject backgroundMainMenu;

    [Header("Fullscreen")]
    private int fullscreenON;
    private string fullscreenPrefsName = "FullscreenPrefs";
    public string[] switchOptions;
    public Text switchText;
    private int switchOptionNumber = 0;

    [Header("Canvas Groups")]
    public CanvasGroup pauseMenuCanvas;
    public CanvasGroup cheatMenuCanvas;
    public CanvasGroup deathMenuCanvas;
    public CanvasGroup winCanvas;

    [Header("Player")]
    public PlayerController playerControl;
    public PlayerAttack playerAttack;
    public EnemyLockOn enemyLockOn;
    public CameraFollow cameraFollow;
    public PlayerDash playerDash;
    public PlayerStamina stamina;
    public PlayerHealth health;

    [Header("Main Menu CanvasGroups")]
    public CanvasGroup[] canvas;

    public static GameManager instance;

    public bool canControls;

    [Header("First Buttons On Menus")]
    public GameObject[] firstButtonFromMenu;

    public GameObject missionTable;

    public AudioClip gameOverClip;
    public AudioClip winClip;
    public AudioSource music;

    [Header("Music Configuration")]
    public AudioSource soundsManagerAudioSource;
    public AudioClip[] audioClipSoundsManager;
    public bool playReloadSound = false;
    private string lastScene;

    public Canvas persistentCanvas;

    public bool isRestarting;
    public bool canUsePauseMenu;
    public bool canBuy;
    public bool canOpenMainMenu;

    [Header("Book Interactions and Transitions")]
    public GameObject bookOpening;
    public GameObject pushAnyButtonCanvas;

    public GameObject firstItemSelect;
    public GameObject buyButton;

    public Text moneyCount;

    public Text firstColorblindnessText;
    public List<string> names;
    private int colorblindOptionNumber = 0;

    private static string[] joysticks = null;

    public GameObject optionsMenuBackButton;
    public GameObject optionsMenuBackButton2;

    #endregion
    #region Unity Events

    private void Awake()
    {
        joysticks = null;

        if (instance == null)
        {
            instance = this;
        }

        canControls = false;

        lastScene = SceneManager.GetActiveScene().name;
    }
    
    private void Start() {

        isRestarting = false;
        canUsePauseMenu = true;
        canBuy = false;
        canOpenMainMenu = true;

        // PLAYER
        if (playerAttack == null)
        {
            Debug.Log("NO_PLAYERATTACK");
        }
        if (playerControl == null)
        {
            Debug.Log("NO_PLAYERCONTROL");
        }

        PopulateListColorblindness();

    }

    private void Update()
    {
        SetMusicVolume();

        if (Input.anyKey && SceneManager.GetActiveScene().name == "MainMenu" && canOpenMainMenu)
        {
            MainMenuActive();
        }

        moneyCount.text = DataManager.instance.data.moneyCount.ToString();

        // Si estoy usando rat�n o mando, cambiar� a verse o no el cursor dependiendo de si est� activo un mando

        joysticks = Input.GetJoystickNames();

        //Si hay alg�n Gamepad activo no se permite el uso de rat�n
        if(joysticks.Length > 0)
        {
            UnableCursor();
        }

        if (SceneManager.GetActiveScene().name == "MainMenu")
        {
            //Si NO hay ning�n Gamepad activo se permite el uso de rat�n en el Main Menu
            if (joysticks.Length == 0)
            {
                EnableCursor();
            }

            persistentCanvas.renderMode = RenderMode.ScreenSpaceCamera;

            //Utils.ActiveCanvasGroup(canvas[0], true);

            Utils.ActiveCanvasGroup(pauseMenuCanvas, false);
            Utils.ActiveCanvasGroup(deathMenuCanvas, false);

            missionTable.SetActive(false);

            backgroundMainMenu.SetActive(true);

            // Se cambia de bot�n BACK en el OptionsMenu para volver al MainMenuCanvas
            optionsMenuBackButton.SetActive(true);
            optionsMenuBackButton2.SetActive(false);

            // Se obliga que el INDEX 0 que es el MAIN MENU CANVAS se inicie en START, al cargar la escena
        }
        else
        {
            // Solo si la pantalla de carga est� a 0 el alpha
            if(canvas[12].alpha == 0)
            {
                missionTable.SetActive(true);
            }
            GameControls();
            backgroundMainMenu.SetActive(false);
            persistentCanvas.renderMode = RenderMode.ScreenSpaceOverlay;

            // Se cambia de bot�n BACK en el OptionsMenu para volver al PauseMenuCanvas
            optionsMenuBackButton.SetActive(false);
            optionsMenuBackButton2.SetActive(true);
        }
        
        SelectionColorblindState();
        SelectionSwitchState();

        // Al haber matado a TODOS los enemigos del TUTORIAL, entonces se consigue la CONDITION y se deja obtener la GEMA DE JADE
        if (DataManager.instance.data.missionCounter == 4)
        {
            DataManager.instance.SetCondition("canTaketheJadeGem", true);
        }
        if (DataManager.instance.data.missionCounter == 10)
        {
            DataManager.instance.SetCondition("canTaketheInfernalGem", true);
        }
        if (DataManager.instance.data.missionCounter == 11)
        {
            DataManager.instance.SetCondition("fireCampFinished", true);
        }
        if (DataManager.instance.data.missionCounter == 11)
        {
            DataManager.instance.SetCondition("castleMission", true);
        }

    }
    #endregion

    #region Methods

    private void PopulateListColorblindness()
    {
        string[] enumNames = Enum.GetNames(typeof(BlindStates));
        names = new List<string>(enumNames);

        firstColorblindnessText.text = names[0];
    }

    public void ChangeColorblindOptions(int sum)
    {
        colorblindOptionNumber += sum;

        if (colorblindOptionNumber < 0)
        {
            colorblindOptionNumber = 8;
            firstColorblindnessText.text = names[colorblindOptionNumber];
        }
        else if (colorblindOptionNumber > 8)
        {
            colorblindOptionNumber = 0;
            firstColorblindnessText.text = names[colorblindOptionNumber];
        }
        else
        {
            firstColorblindnessText.text = names[colorblindOptionNumber];
        }

    }
    private void SelectionColorblindState()
    {
        if (colorblindOptionNumber == 0)
        {
            Colorblind_Selector.instance.ChangeStates(BlindStates.Normal);
        }
        else if (colorblindOptionNumber == 1)
        {
            Colorblind_Selector.instance.ChangeStates(BlindStates.Achromatomaly);
        }
        else if (colorblindOptionNumber == 2)
        {
            Colorblind_Selector.instance.ChangeStates(BlindStates.Achromatopsia);
        }
        else if (colorblindOptionNumber == 3)
        {
            Colorblind_Selector.instance.ChangeStates(BlindStates.Deuteranomaly);
        }
        else if (colorblindOptionNumber == 4)
        {
            Colorblind_Selector.instance.ChangeStates(BlindStates.Deuteranopia);
        }
        else if (colorblindOptionNumber == 5)
        {
            Colorblind_Selector.instance.ChangeStates(BlindStates.Protanomaly);
        }
        else if (colorblindOptionNumber == 6)
        {
            Colorblind_Selector.instance.ChangeStates(BlindStates.Protanopia);
        }
        else if (colorblindOptionNumber == 7)
        {
            Colorblind_Selector.instance.ChangeStates(BlindStates.Tritanomaly);
        }
        else if (colorblindOptionNumber == 8)
        {
            Colorblind_Selector.instance.ChangeStates(BlindStates.Tritanopia);
        }
    }
    public void ChangeOnOffOptions(int sum)
    {
        switchOptionNumber += sum;

        if (switchOptionNumber < 0)
        {
            switchOptionNumber = 1;
            switchText.text = switchOptions[switchOptionNumber];
        }
        else if (switchOptionNumber > 1)
        {
            switchOptionNumber = 0;
            switchText.text = switchOptions[switchOptionNumber];
        }
        else
        {
            switchText.text = switchOptions[switchOptionNumber];
        }

    }
    private void SelectionSwitchState()
    {
        if (switchOptionNumber == 0)
        {
            Fullscreen(false);
        }
        if (switchOptionNumber == 1)
        {
            Fullscreen(true);
        }
    }
    public void ChangeMenuScene(CanvasGroup canvasGroup)
    {

        foreach (CanvasGroup child in canvas)
        {
            if (child == canvasGroup)
            {
                Utils.ActiveCanvasGroup(canvasGroup, true);

                // Siempre que se active el MainMenuCanvas se selecciona el bot�n "Play" SOLO si hay mando conectado
                if (canvasGroup.gameObject.name == "MainMenuCanvas")
                {
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstButtonFromMenu[0]);
                }
                if (canvasGroup.gameObject.name == "InstructionsMenuCanvas")
                {
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstButtonFromMenu[1]);
                }
                if (canvasGroup.gameObject.name == "GamepadControlsMenuCanvas")
                {
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstButtonFromMenu[2]);
                }
                if (canvasGroup.gameObject.name == "PCControlsMenuCanvas")
                {
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstButtonFromMenu[3]);
                }
                if (canvasGroup.gameObject.name == "OptionsMenuCanvas")
                {
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstButtonFromMenu[4]);
                }
                if (canvasGroup.gameObject.name == "SoundsMenuCanvas")
                {
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstButtonFromMenu[5]);
                }
                if (canvasGroup.gameObject.name == "CreditsMenuCanvas")
                {
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstButtonFromMenu[6]);
                }
                if (canvasGroup.gameObject.name == "PauseMenuCanvas")
                {
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstButtonFromMenu[7]);
                }
                if (canvasGroup.gameObject.name == "DeathMenuCanvas")
                {
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstButtonFromMenu[8]);
                }
                if (canvasGroup.gameObject.name == "TravelMapCanvas")
                {
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstButtonFromMenu[9]);
                }
                if (canvasGroup.gameObject.name == "ShopCanvas")
                {
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstButtonFromMenu[10]);
                }
                if (canvasGroup.gameObject.name == "TravelToGalapua")
                {
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstButtonFromMenu[12]);
                }
                if (canvasGroup.gameObject.name == "TravelToFireCamp")
                {
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstButtonFromMenu[13]);
                }
                if (canvasGroup.gameObject.name == "TravelToCastle")
                {
                    EventSystem.current.SetSelectedGameObject(null);
                    EventSystem.current.SetSelectedGameObject(firstButtonFromMenu[14]);
                }
            }
            else
            {
                Utils.ActiveCanvasGroup(child, false);
            }
        }
    }
    /// <summary>
    /// Despausa la partida
    /// </summary>
    public void Resume()
    {
        Time.timeScale = 1;

        Utils.ActiveCanvasGroup(pauseMenuCanvas, false);

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        PlayerController.instance.canMove = true;

        if (playerAttack != null)
        {
            playerAttack.enabled = true;
        }
        if(playerControl != null)
        {
            playerControl.enabled = true;
        }
        if(playerDash != null)
        {
            playerDash.enabled = true;
        }
        if(enemyLockOn != null)
        {
            enemyLockOn.enabled = true;
        }
        if(cameraFollow != null)
        {
            cameraFollow.enabled = true;
        }


    }
    /// <summary>
    /// Reinicia la partida
    /// </summary>
    public void Restart(string sceneName)
    {
        // Si hay una escena PERSISTENT al obtener las escenas ACTIVAS por el "name" y no por "BuildIndex"
        // se consigue reiniciar TODAS las escenas activas en juego

        SceneManager.LoadScene(sceneName);

        SceneController.instance.FadeAndLoadScene(DataManager.instance.data.currentScene, "");

        Utils.ActiveCanvasGroup(canvas[0], true);
    }
    /// <summary>
    /// Salir del juego
    /// </summary>
    public void Exit()
    {
        Application.Quit();
    }
    /// <summary>
    /// Cierra el mapa de transporte
    /// </summary>
    public void CloseTravelMap()
    {
        UnableCanvas(canvas[4]);

        StartCoroutine(TransportUI.instance.DelayOnTrigger());

        TransportUI.instance.canUseMap = false;
    }
    /// <summary>
    /// Desactiva el Canvas usando un bot�n
    /// </summary>
    /// <param name="menu"></param>
    public void UnableCanvas(CanvasGroup menu)
    {
        Time.timeScale = 1f;
        Utils.ActiveCanvasGroup(menu, false);

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        if(cameraFollow != null)
        {
            cameraFollow.enabled = true;
        }
        if(playerControl != null)
        {
            PlayerController.instance.canMove = true;
        }

        //Al desactivar el canvas tambi�n se desactiva el bot�n seleccionado y se pasa a NULL
        EventSystem.current.SetSelectedGameObject(null);
    }

    public void GameControls() {

        if (canUsePauseMenu)
        {
            if (Input.GetButtonDown("Pause"))
            {
                bool isPaused = Time.timeScale == 0 ? false : true;

                Pause(isPaused);
            }
        }
    }

    /// <summary>
    /// Pausa o reanuda la partida
    /// </summary>
    /// <param name="paused"></param>
    public void Pause(bool paused) {
 
        // Si se pide pausar, se detendr� el contador de tiempo
        Time.timeScale = paused ? 0 : 1;
        // Se activa o se desactiva el men� seg�n se solicita
        if (pauseMenuCanvas != null)
        {
            ChangeMenuScene(canvas[10]);
            Utils.ActiveCanvasGroup(pauseMenuCanvas, true);
        }
        else
        {
            Debug.Log("No hay pauseMenuCanvas en escena");
        }

        if (paused)
        {
            if (joysticks.Length > 0)
            {
                UnableCursor();
            }
            else
            {
                EnableCursor();
            }

            PlayerController.instance.canMove = false;

            if(enemyLockOn != null)
            {
                enemyLockOn.enabled = false;
            }
            if(cameraFollow != null)
            {
                cameraFollow.enabled = false;
            }
            StartCoroutine(DelayOnPause());
        }
    }
    /// <summary>
    /// Activa el menu de game over
    /// </summary>
    public void DeathMenu()
    {
        //StartCoroutine(AudioPlayer());

        if (joysticks.Length > 0)
        {
            UnableCursor();
        }
        else
        {
            EnableCursor();
        }

        if (enemyLockOn != null)
        {
            enemyLockOn.enabled = false;
        }
        if (cameraFollow != null)
        {
            cameraFollow.enabled = false;
        }

        if (deathMenuCanvas != null)
        {
            //Utils.ActiveCanvasGroup(deathMenuCanvas, true);
            ChangeMenuScene(canvas[16]);
        }
        else
        {
            Debug.Log("No hay deathMenuCanvas en escena");
        }

    }

    public void SetMusicVolume()
    {
        
        string currentScene = SceneManager.GetActiveScene().name;

        if(currentScene != lastScene || !soundsManagerAudioSource.isPlaying)
        {
            lastScene = currentScene;

            playReloadSound = false;
        }
        else
        {
            playReloadSound = true;
        }

        if (SceneManager.GetActiveScene().name == "MainMenu")
        {

            if (playReloadSound == false)
            {
                soundsManagerAudioSource.Stop();
                soundsManagerAudioSource.PlayOneShot(audioClipSoundsManager[0]);
            }

        }
        if (SceneManager.GetActiveScene().name == "DarkForest")
        {

            if (playReloadSound == false)
            {
                soundsManagerAudioSource.Stop();
                soundsManagerAudioSource.PlayOneShot(audioClipSoundsManager[0]);
            }

        }
        if (SceneManager.GetActiveScene().name == "GalapuaVillage")
        {

            if (playReloadSound == false)
            {
                soundsManagerAudioSource.Stop();
                soundsManagerAudioSource.PlayOneShot(audioClipSoundsManager[1]);
            }

        }
        if (SceneManager.GetActiveScene().name == "FireCamp 1")
        {

            if (playReloadSound == false)
            {
                soundsManagerAudioSource.Stop();
                soundsManagerAudioSource.PlayOneShot(audioClipSoundsManager[2]);
            }

        }
        if (SceneManager.GetActiveScene().name == "Dungeon")
        {

            if (playReloadSound == false)
            {
                soundsManagerAudioSource.Stop();
                soundsManagerAudioSource.PlayOneShot(audioClipSoundsManager[3]);
            }

        }
        if (SceneManager.GetActiveScene().name == "ThroneChamber")
        {

            if (playReloadSound == false)
            {
                soundsManagerAudioSource.Stop();
                soundsManagerAudioSource.PlayOneShot(audioClipSoundsManager[3]);
            }

        }

    }

    /// <summary>
    /// Activa/Desactiva el modo Pantalla Completa
    /// </summary>
    /// <param name="isFullscreen"></param>
    public void Fullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }
    /// <summary>
    /// Carga el DATA de la memoria
    /// </summary>
    public void LoadData()
    {
        fullscreenON = PlayerPrefs.GetInt("FullscreenPrefs");
    }

    /// <summary>
    /// Carga el main menu canvas
    /// </summary>
    public IEnumerator ActivateMainMenuCanvas()
    {
        yield return new WaitForSeconds(1.8f);

        Utils.ActiveCanvasGroup(canvas[0], true);

        canvas[0].gameObject.SetActive(true);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(firstButtonFromMenu[0]);

        if(SceneManager.GetActiveScene().name != "MainMenu")
        {
            InventoryManager.instance.CloseInventory();
        }

        TextManager.instance.HideMessage();

        if (canvas[17].alpha == 1)
        {
            DataManager.instance.DeleteSaveState();
        }
    }

    /// <summary>
    /// Carga el options menu canvas
    /// </summary>
    public IEnumerator ActivateOptionMenuCanvas()
    {
        yield return new WaitForSeconds(1.8f);

        Utils.ActiveCanvasGroup(canvas[2], true);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(firstButtonFromMenu[4]);

        bookOpening.SetActive(false);

        pushAnyButtonCanvas.SetActive(false);

        if(SceneManager.GetActiveScene().name != "MainMenu")
        {
            InventoryManager.instance.CloseInventory();
        }

        TextManager.instance.HideMessage();

    }
    public void ActivateMainMenu()
    {
        StartCoroutine(ActivateMainMenuCanvas());
    }
    public void ActivateOptionsMenu()
    {
        StartCoroutine(ActivateOptionMenuCanvas());
    }
    public void ActivateMenu(CanvasGroup canvas)
    {
        Utils.ActiveCanvasGroup(canvas, true);

        if (joysticks.Length > 0)
        {
            UnableCursor();
        }
        else
        {
            EnableCursor();
        }

        if (enemyLockOn != null)
        {
            enemyLockOn.enabled = false;
        }
        if (cameraFollow != null)
        {
            cameraFollow.enabled = false;
        }
        if (playerAttack != null)
        {
            playerAttack.enabled = false;
        }
        if (playerControl != null)
        {
            playerControl.enabled = false;
        }
        Time.timeScale = 0f;
    }
    /// <summary>
    /// Habilita la pantalla de victoria
    /// </summary>
    public void WinMenu()
    {
        Utils.ActiveCanvasGroup(winCanvas, true);
        music.PlayDelayed(2f);
        music.PlayOneShot(winClip);
        missionTable.SetActive(false);

        if (Input.GetJoystickNames().Length > 0)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }

    /// <summary>
    /// Al pulsar cualquier tecla se ejecuta la animaci�n
    /// </summary>
    public void MainMenuActive()
    {
        Utils.ActiveCanvasGroup(canvas[0], true);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(firstButtonFromMenu[0]);

        bookOpening.SetActive(false);

        canOpenMainMenu = false;
    }
    /// <summary>
    /// Guarda la partida de juego
    /// </summary>
    public void SaveGame()
    {
        DataManager.instance.Save();
    }
    /// <summary>
    /// Borra la partida de juego
    /// </summary>
    public void DeleteSaveGame()
    {
        DataManager.instance.DeleteSaveState();
    }
    /// <summary>
    /// Activa el MainMenu OBJECT 
    /// </summary>
    /// <param name="active"></param>
    public void SetActiveMainMenu(bool active)
    {
        backgroundMainMenu.SetActive(active);
    }
    /// <summary>
    /// Inicializaci�n de la tienda de compra
    /// </summary>
    public void Shop()
    {
        Utils.ActiveCanvasGroup(canvas[7], true);

        if (Input.GetJoystickNames().Length > 0)
        {
            EventSystem.current.SetSelectedGameObject(firstItemSelect);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

        PlayerController.instance.canMove = false;

        cameraFollow.enabled = false;

        StartCoroutine(DelayOnBuy());
    }
    /// <summary>
    /// Abrir la opci�n de Comprar Objeto
    /// </summary>
    public void OpenShop()
    {
        if(canBuy == true)
        {
            if(Input.GetJoystickNames().Length > 0)
            {
                EventSystem.current.SetSelectedGameObject(buyButton);
            }

            Utils.ActiveCanvasGroup(canvas[8], true);
            Utils.ActiveCanvasGroupNoAlpha(canvas[7], false);
        }
    }
    /// <summary>
    /// Cierra el men� de compras
    /// </summary>
    public void CloseShopMenu()
    {
        UnableCanvas(canvas[7]);

        canBuy = false;
    }
    /// <summary>
    /// Cerrar la opci�n de Comprar Objeto
    /// </summary>
    public void CloseItemBuy()
    {
        if(Input.GetJoystickNames().Length > 0)
        {
            EventSystem.current.SetSelectedGameObject(firstItemSelect);
        }

        Utils.ActiveCanvasGroup(canvas[8], false);
        Utils.ActiveCanvasGroupNoAlpha(canvas[7], true);
    }
    /// <summary>
    /// Corrutina que hace que suene el sonido de muerte
    /// </summary>
    /// <returns></returns>
    public IEnumerator AudioPlayer()
    {
        music.PlayOneShot(gameOverClip);

        yield return new WaitForSeconds(7f);

        music.Stop();

    }
    /// <summary>
    /// Retraso a la hora de activar el men� de pausa
    /// </summary>
    /// <returns></returns>
    public IEnumerator DelayOnPause()
    {
        canUsePauseMenu = false;

        yield return new WaitForSeconds(2f);

        canUsePauseMenu = true;
    }
    /// <summary>
    /// Retraso para una selecci�n correcta de primer bot�n en la tienda
    /// </summary>
    /// <returns></returns>
    public IEnumerator DelayOnBuy()
    {
        canBuy = false;

        yield return new WaitForSeconds(0.1f);

        canBuy = true;
    }
    /// <summary>
    /// Retraso para una selecci�n correcta de primer bot�n en la tienda
    /// </summary>
    /// <returns></returns>
    public IEnumerator DelayOnPlayButton()
    {
        canOpenMainMenu = false;

        yield return new WaitForSeconds(1.08f);

        MainMenuActive();

        canOpenMainMenu = false;
    }
    public void UnableCursor()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
    public void EnableCursor()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
    }
    #endregion
}
