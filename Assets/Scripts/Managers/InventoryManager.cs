using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.Linq;
//using static UnityEditor.Progress;

public class InventoryManager : MonoBehaviour
{
    #region VARIABLES
    public GameObject inventoryCanvas;
    // Instancia para singleton
    public static InventoryManager instance;

    public List<Image> itemInventorySlots;
    public List<Text> itemInventoryAmounts;
    public List<Text> itemOwnedShop;

    public Text moneyInventoryText;

    public bool isOpened;
    #endregion
    #region UNITY EVENTS
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        UpdateInventory();

        isOpened = false;
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene().name != "MainMenu")
        {
            Controls();

            /*
            if (GameManager.instance.canvas[9].alpha == 1)
            {
                PlayerController.instance.canMove = false;
            }
            */
        }
        else
        {
            Utils.ActiveCanvasGroup(GameManager.instance.canvas[9], false);
        }


    }

    #endregion
    #region METHODS
    public void Controls()
    {
        // Toggle del HUD

        if (Input.GetButtonDown("Inventory"))
        {
            OpenInventory();
        }

    }
    public void OpenInventory()
    {

        Utils.ActiveCanvasGroup(GameManager.instance.canvas[9], true);

        moneyInventoryText.text = DataManager.instance.data.moneyCount.ToString();

        if (GameManager.instance.cameraFollow != null)
        {
            GameManager.instance.cameraFollow.enabled = false;
        }
        PlayerController.instance.anim.SetFloat("Forward", 0f);
        PlayerController.instance.anim.SetFloat("Right", 0f);

        if (DataManager.instance.data.missions[4].isDone)
        {
            TextManager.instance.HideMessage();
        }

        if (Input.GetJoystickNames().Length > 0)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        isOpened = true;

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(GameManager.instance.firstButtonFromMenu[9]);
    }
    /// <summary>
    /// Actualiza visualmente los elementos del inventario
    /// </summary>
    public void UpdateInventory()
    {
        // Se recorren todos los Slots del inventario
        for (int i = 0; i < DataManager.instance.data.inventory.Length; i++)
        {
            // Se recupera el image del item correspondiente en la jerarqu�a en el Inspector
            Image tempImage = itemInventorySlots[i];

            Text tempAmount = itemInventoryAmounts[i];

            // Confirmamos que existe un item en dicho slot
            if (DataManager.instance.data.inventory[i].name != "")
            {
                // Cargamos la imagen correspondiente en el slot
                tempImage.sprite = Resources.Load<Sprite>("Images/Icons/" + DataManager.instance.data.inventory[i].imageName);
                // Se activa la imagen para que sea visible
                tempImage.gameObject.SetActive(true);

                tempAmount.text = DataManager.instance.data.inventory[i].amount.ToString();

                if(DataManager.instance.data.inventory[i].name == "Arrow")
                {
                    itemOwnedShop[0].text = tempAmount.text;
                }
                if (DataManager.instance.data.inventory[i].name == "Potion")
                {
                    itemOwnedShop[1].text = tempAmount.text;
                }
                if (DataManager.instance.data.inventory[i].name == "Celeste")
                {
                    itemOwnedShop[2].text = tempAmount.text;
                }
            }
            else
            {
                tempImage.gameObject.SetActive(false);
                tempAmount.text = DataManager.instance.data.inventory[i].amount.ToString();
            }
        }
    }
    /// <summary>
    /// Agrega un elemento al inventario
    /// </summary>
    /// <param name="itemName"></param>
    /// <returns></returns>
    public bool AddItemInventory(string itemName)
    {
        // Variable donde se almacenar� la informaci�n del item a a�adir
        BinaryItem newItem = new BinaryItem();

        BinaryItem item = DataManager.instance.data.allItems.Where(i => i.name == itemName).FirstOrDefault();

        if (item != null)
        {
            // Volcamos la informaci�n al new item
            newItem.name = item.name;
            newItem.description = item.description;
            newItem.imageName = item.imageName;
            newItem.picked = item.picked;
        }

        for (int i = 0; i < DataManager.instance.data.inventory.Length; i++)
        {
            if(DataManager.instance.data.inventory[i].name != "")
            {
                if (DataManager.instance.data.inventory[i].name == "Arrow" && itemName == "Arrow")
                {
                    if(DataManager.instance.data.inventory[i].amount < DataManager.instance.data.maxArrow)
                    {
                        DataManager.instance.data.inventory[i].amount++;

                        UpdateInventory();

                        DataManager.instance.Save();

                        return true;
                    }

                }
                if (DataManager.instance.data.inventory[i].name == "Potion" && itemName == "Potion")
                {
                    DataManager.instance.data.inventory[i].amount++;

                    DataManager.instance.data.potionAmount++;

                    UpdateInventory();

                    DataManager.instance.Save();

                    return true;
                }

            }
            else
            {
                // Se almacena ah� el item
                DataManager.instance.data.inventory[i] = newItem;

                DataManager.instance.data.inventory[i].amount++;

                UpdateInventory();

                // Devuelve true, al haber podido recoger el objeto
                return true;

            }

        }

        // No hab�a hueco en el inventario y no se ha podido recoger el objeto
        return false;
    }
    /// <summary>
    /// Elimina el item cuyo nombre recibe como par�metro
    /// </summary>
    /// <param name="itemName"></param>
    public bool RemoveItemInventory(string itemName)
    {
        // Se recorren todos los slots
        for (int i = 0; i < DataManager.instance.data.inventory.Length; i++)
        {
            // Si el nombre del item coincide con el buscado
            if (DataManager.instance.data.inventory[i].name == itemName)
            {
                if (DataManager.instance.data.inventory[i].amount > 1)
                {
                    DataManager.instance.data.inventory[i].amount--;

                    UpdateInventory();

                    DataManager.instance.Save();

                    return true;
                }
                else
                {
                    DataManager.instance.data.inventory[i].name = "";
                    DataManager.instance.data.inventory[i].description = "";
                    DataManager.instance.data.inventory[i].imageName = "";
                    DataManager.instance.data.inventory[i].picked = false;
                    DataManager.instance.data.inventory[i].amount--;

                    UpdateInventory();

                    return true;
                }
            }
        }
        return false;
    }
    public void CloseInventory()
    {
        Utils.ActiveCanvasGroup(GameManager.instance.canvas[9], false);

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        isOpened = false;

        PlayerController.instance.canMove = true;

        GameManager.instance.cameraFollow.enabled = true;
    }
    #endregion
}

