using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;
//using static UnityEditor.Progress;

public class InventoryManagerBACKUP : MonoBehaviour
{
    #region Variables
    public static InventoryManagerBACKUP instance;

    public GameObject inventoryHud;

    /*[Header("Gem Slots")]
    public Transform[] gems;
    [Header("Other Slots")]
    public Transform[] otherItems;*/

    public List<Image> itemSlotImages;
    public List<Image> itemInventorySlots;
    public List<Text> itemInventoryAmounts;

    public Text potionAmountText;

    public Text moneyInventoryText;

    public bool isOpened;

    private int counter = 0;

    public Image fireGem;
    public Image jadeGem;
    public Image arrow;
    #endregion

    #region Unity Events
    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        //InitInventoryGems();
        //InitInventoryItems();

        // Se inicializa con las images del Inventario en FALSE, y se activan cuando se consigue un objeto 
        foreach (Image item in itemInventorySlots)
        {
            item.enabled = false;
        }

        isOpened = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (SceneManager.GetActiveScene().name != "MainMenu")
        {
            Controls();
        }
        else
        {
            Utils.ActiveCanvasGroup(GameManager.instance.canvas[9], false);
        }

    }

    #endregion

    #region Methods
    /*public void EquipItemBySlot(int slot)
    {
        switch (slot)
        {
            case 1:
                for (int i = 0; i < gems.Length; i++)
                {
                    glowUpEquippedItem(gems[i], i == 0);
                }
                break;
            case 2:
                for (int i = 0; i < gems.Length; i++)
                {
                    glowUpEquippedItem(gems[i], i == 1);
                }
                break;
            case 3:
                for (int i = 0; i < gems.Length; i++)
                {
                    glowUpEquippedItem(gems[i], i == 2);
                }
                break;
            case 4:
                // TODO: Efecto visual, algo as� como un blink
                break;
            default:
                Debug.Log("No se ha encontrado el item: " + slot);
                break;
        }
    }*/

    public void glowUpEquippedItem(Transform itemSlot, bool glowUp)
    {
        Color color = glowUp ? Color.yellow : Color.white;
        itemSlot.GetComponent<Image>().color = color;
    }

    public void ConsumeItem(Transform itemSlot)
    {
        //TODO: Logica para consumir el item
    }

    public void Controls()
    {
        // Toggle del HUD

        if (Input.GetButtonDown("Inventory"))
        {
            OpenInventory();
        }

    }

    public void ActiveItem(Transform itemSlot, bool active)
    {
        foreach (Transform child in itemSlot)
        {
            if (child.name == "ItemIcon")
            {
                child.gameObject.SetActive(active);
            }
        }
    }

    public bool ItemStored(Transform itemSlot)
    {
        foreach (Transform child in itemSlot)
        {
            if (child.name == "ItemIcon")
            {
                return child.gameObject.activeSelf;
            }
        }
        return false;
    }

    /*
    public void InitInventoryGems()
    {
        foreach (Transform item in gems)
        {
            ActiveItem(item, DataManager.instance.CheckPickedItem(item.name.Split('_')[0]));
        }
    }

    public void InitInventoryItems()
    {
        foreach (Transform item in otherItems)
        {
            switch (item.name.Split('_')[0])
            {
                case "Potion":
                    potionAmountText.text = DataManager.instance.GetItemCount(item.name.Split('_')[0]).ToString();
                    break;
                default:
                    Debug.Log("No se ha encontrado el item: " + item);
                    break;
            }
        }
    }
    */
    public void OpenInventory()
    {
        Utils.ActiveCanvasGroup(GameManager.instance.canvas[9], true);

        moneyInventoryText.text = DataManager.instance.data.moneyCount.ToString();

        if (DataManager.instance.data.missions[4].isDone)
        {
            TextManager.instance.HideMessage();
        }

        if (Input.GetJoystickNames().Length > 0)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        isOpened = true;

        PlayerController.instance.canMove = false;

        GameManager.instance.cameraFollow.enabled = false;
    }

    public void CloseInventory()
    {
        Utils.ActiveCanvasGroup(GameManager.instance.canvas[9], false);

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        isOpened = false;

        PlayerController.instance.canMove = true;

        GameManager.instance.cameraFollow.enabled = true;
    }

    public void SaveItemOnInventory(Image itemImageFromButton)
    {

        if (itemSlotImages.Count >= 0)
        {
            itemSlotImages.Add(itemImageFromButton);
        }

    }

    public void ChangeInventorySlotImage()
    {
        foreach (Image itemSlot in itemInventorySlots)
        {
            if (itemSlot.enabled == false)
            {
                if (itemSlot.name == "ItemIcon1")
                {
                    itemSlot.sprite = itemSlotImages[0].sprite;
                }
                if (itemSlot.name == "ItemIcon2")
                {
                    itemSlot.sprite = itemSlotImages[1].sprite;
                }
                if (itemSlot.name == "ItemIcon3")
                {
                    itemSlot.sprite = itemSlotImages[2].sprite;
                }
                if (itemSlot.name == "ItemIcon4")
                {
                    itemSlot.sprite = itemSlotImages[3].sprite;
                }
                if (itemSlot.name == "ItemIcon5")
                {
                    itemSlot.sprite = itemSlotImages[4].sprite;
                }
                if (itemSlot.name == "ItemIcon6")
                {
                    itemSlot.sprite = itemSlotImages[5].sprite;
                }
            }

            //DataManager.instance.data.allItems[0].

            itemSlot.enabled = true;

        }

    }
    /*
    public void CheckCounter()
    {

        for (int i = 0; i < itemSlotImages.Count; i++)
        {
            if (itemSlotImages[i].name == "Potion")
            {
                itemInventoryAmounts[i].text = ShopItemBuy.instance.counterForPotions.ToString();
            }
            if (itemSlotImages[i].name == "Arrow")
            {
                itemInventoryAmounts[i].text = ShopItemBuy.instance.counterForArrow.ToString();
            }
        }
    }*/
    #endregion
}
