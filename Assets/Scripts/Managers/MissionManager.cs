using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using UnityEngine.SceneManagement;

public class MissionManager : MonoBehaviour
{
    [Header("Texto para la misi�n a realizar")]
    public Text text;
    public GameObject missionPanel;

    public static MissionManager instance;

    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }

    }
    void Start()
    {
        StartCoroutine(DelayMissionStart());

        text.text = DataManager.instance.data.missions[DataManager.instance.data.missionCounter].description;
    }
    private void Update()
    {
        if (SceneManager.GetActiveScene().name == "GalapuaVillage")
        {
            missionPanel.SetActive(true);
        }
        else
        {
            return;
        }
        text.text = DataManager.instance.data.missions[DataManager.instance.data.missionCounter].description;
    }

    private void OnEnable()
    {
        /*FPSController.OnMove += () => CheckMission("mission2", 1);

        EnemyHealth.OnDead += () => CheckMission("mission6", 1);

        EnemyHealth.OnDeadBoss += () => CheckMission("mission8", 1);*/
    }
    /// <summary>
    /// Se realiza un delay al comienzo de partida para cargar la primera misi�n
    /// </summary>
    /// <returns></returns>
    private IEnumerator DelayMissionStart()
    {
        yield return new WaitUntil(() => SceneManager.GetActiveScene().name == "DarkForest");
        // TIENE QUE RECOGER LA ULTIMA MISION QUE QUED� sin marcar, o sea !m.isDone

        CheckMission("mission1", 0);
    }
    /// <summary>
    /// Se comprueba las misiones, su estado y las cantidades necesarias
    /// </summary>
    /// <param name="missionID"></param>
    /// <param name="amount"></param>
    public void CheckMission(string missionID, int amount)
    {

        Stat missionStat = DataManager.instance.data.statistics.Where(s => s.code == missionID).FirstOrDefault();

        //Si NO existe se sale del m�todo
        if (missionStat == null) return;

        missionStat.value += amount;

        foreach (Mission mission
            in DataManager.instance.data.missions.Where(m => m.missionID == missionID && !m.isDone).AsEnumerable())
        {
            mission.description = TranslateManager.instance.GetString(missionID);

            if (missionStat.value >= mission.targetAmount)
            {
                mission.isDone = true;
                /*
                if (missionID == "mission6")
                {
                    DataManager.instance.SetCondition("enemyDead", true);
                }
                if (missionID == "mission8" && PlayerHealth.instance.currentLife == PlayerHealth.instance.maxLife && mission.isDone)
                {
                    OnAllHealth?.Invoke();
                }*/
            }

            if (!mission.isDone)
            {
                text.text = mission.description;
            }
            else
            {
                DataManager.instance.data.missionCounter++;
            }
            

            DataManager.instance.Save();

        }

    }

}
