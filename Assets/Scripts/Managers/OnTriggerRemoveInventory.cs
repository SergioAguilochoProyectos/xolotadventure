using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerRemoveInventory : MonoBehaviour
{
    public string itenName;
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            InventoryManager.instance.RemoveItemInventory(itenName);
        }
    }
}
