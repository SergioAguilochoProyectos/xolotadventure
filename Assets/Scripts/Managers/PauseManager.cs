using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : MonoBehaviour
{
    public PlayerAttack attack;
    public PlayerController controller;
    public EnemyLockOn locker;
    public CameraFollow cameraFollow;
    public PlayerDash dash;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.playerControl = controller;
        GameManager.instance.playerAttack = attack;
        GameManager.instance.enemyLockOn = locker;
        GameManager.instance.cameraFollow = cameraFollow;
        GameManager.instance.playerDash = dash;
    }
}
