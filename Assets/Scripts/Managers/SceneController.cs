using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneController : MonoBehaviour
{
    #region VARIABLES
    // Canvas usado para el fundido en negro
    public CanvasGroup fadeCanvasGroup;
    // Duraci�n del fade
    public float fadeDuration = 1f;
    // Configuraci�n inicial de escena y de posici�n
    private string startingScene;
    private string initialStartingPosition;

    public Animator loadingAnim;

    public static SceneController instance;

    #endregion
    #region UNITY EVENTS
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

    }

    private IEnumerator Start()
    {
        // Pantalla en negro al iniciar el juego
        fadeCanvasGroup.alpha = 1f;
        // Recoge la informaci�n almacenada en el Data Manager
        startingScene = DataManager.instance.data.currentScene;
        initialStartingPosition = DataManager.instance.data.entrancePosition;

        yield return StartCoroutine(LoadSceneAsync("MainMenu"));

        StartCoroutine(Fade(0f));

        yield return null;
    }
    #endregion
    #region METHODS
    /// <summary>
    /// Realiza un fundido a visible o invisible
    /// </summary>
    /// <param name="finalAlpha"></param>
    /// <returns></returns>
    private IEnumerator Fade(float finalAlpha)
    {
        // Se bloquean las interacciones
        fadeCanvasGroup.blocksRaycasts = true;
        // Se inicializa el contador de tiempo y el alpha inicial del CanvasGroup
        float timerCounter = fadeDuration;
        float initialAlpha = fadeCanvasGroup.alpha;

        while (timerCounter > 0)
        {
            // Mathf.Lerp = Se realiza un Lerp/interpolaci�n pero num�rico por "Mathf"
            fadeCanvasGroup.alpha = Mathf.Lerp(initialAlpha, finalAlpha, 1f - timerCounter / fadeDuration);
            timerCounter -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        // Por si el resultado final no es el valor exacto, se quedar� con el valor final de Alpha, realizando esta asignaci�n
        fadeCanvasGroup.alpha = finalAlpha;
        // Se deja de bloquear
        fadeCanvasGroup.blocksRaycasts = false;
    }
    [ContextMenu("Fade Test")]
    public void FadeTest()
    {
        if (fadeCanvasGroup.alpha == 0f)
        {
            //1f = finalAlpha
            StartCoroutine(Fade(1f));
        }
        else
        {
            StartCoroutine(Fade(0f));
        }
    }
    /// <summary>
    /// Carga una escena de forma as�ncrona
    /// </summary>
    /// <param name="sceneName"></param>
    /// <returns></returns>
    private IEnumerator LoadSceneAsync(string sceneName)
    {
        // Si no se ha indicado la escena que cargar
        if (sceneName == "")
        {
            Debug.LogWarning("No se ha indicado una escena que cargar");
            yield break;
        }
        // Se carga la escena de forma aditiva, sin destruir la escena principal
        yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        // Se fuerza la actualizaci�n de la informaci�n de las LightProbes de la escena
        LightProbes.Tetrahedralize();
        // Se recupera la �ltima escena cargada
        Scene newScene = SceneManager.GetSceneAt(SceneManager.sceneCount - 1);
        // Marcamos la nueva escena como la escena activa
        SceneManager.SetActiveScene(newScene);
    }
    /// <summary>
    /// Realiza un fade in, descarga de la escena actual, carga la nueva y fade out
    /// </summary>
    /// <param name="sceneName"></param>
    /// <returns></returns>
    private IEnumerator FadeAndSwitchScenes(string sceneName)
    {
        /* Se usa el yield return para que HASTA QUE NO se haya descargado la anterior escena no va a la siguiente, (va por tramos) 
        sino se estar�a descargando y cargando "A LA VEZ" */
        // Fade out de la escena
        yield return StartCoroutine(Fade(1f));
        // Recoge esta escena activa y se descarga
        yield return SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().buildIndex);
        // Carga de la nueva escena cuyo nombre se recibe como par�metro
        yield return StartCoroutine(LoadSceneAsync(sceneName));
        // Fade in de la nueva escena
        yield return StartCoroutine(Fade(0f));
    }
    /// <summary>
    /// Llamada p�blica para el cambio de escena
    /// </summary>
    /// <param name="sceneName"></param>
    /// <param name="entrancePosition"></param>
    public void FadeAndLoadScene(string sceneName, string entrancePosition)
    {
        // Se actualizan los valores de la escena actual
        DataManager.instance.data.currentScene = sceneName;
        DataManager.instance.data.entrancePosition = entrancePosition;

        // Se guarda la partida al realiza un cambio de escena
        DataManager.instance.Save();

        StartCoroutine(FadeAndSwitchScenes(sceneName));
    }
    #endregion
}
