using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SoundsManager : MonoBehaviour
{
    [Header("Music & Sounds")]
    public AudioMixer audioMixer;
    public Slider[] gameVolumes;

    /// <summary>
    /// Carga el DATA de la memoria
    /// </summary>
    public void LoadData()
    {
        foreach (Slider sliderVolume in gameVolumes)
        {
            switch (sliderVolume.name)
            {
                case "MusicVolume":
                    sliderVolume.value = PlayerPrefs.GetFloat("MusicVolume");
                    break;
                case "SoundVolume":
                    sliderVolume.value = PlayerPrefs.GetFloat("SoundsVolume");
                    break;
                case "TorchVolume":
                    sliderVolume.value = PlayerPrefs.GetFloat("TorchVolume");
                    break;
                case "PlayerVolume":
                    sliderVolume.value = PlayerPrefs.GetFloat("PlayerVolume");
                    break;
                case "BossVolume":
                    sliderVolume.value = PlayerPrefs.GetFloat("BossVolume");
                    break;
                default:
                    Debug.LogError("No audio ###");
                    break;
            }
        }
    }

    /// <summary>
    /// Configura el volumen de la m�sica
    /// </summary>
    /// <param name="volume"></param>
    public void SetMusic(float volume)
    {
        PlayerPrefs.SetFloat("MusicVolume", volume);
        audioMixer.SetFloat("MusicVolume", volume);
    }
    /// <summary>
    /// Configura el volumen del sonido
    /// </summary>
    /// <param name="volume"></param>
    public void SetSound(float volume)
    {
        PlayerPrefs.SetFloat("SoundsVolume", volume);
        audioMixer.SetFloat("SoundsVolume", volume);
    }
    /// <summary>
    /// Configura el volumen del sonido de antorcha
    /// </summary>
    /// <param name="volume"></param>
    public void SetSoundTorch(float volume)
    {
        PlayerPrefs.SetFloat("TorchVolume", volume);
        audioMixer.SetFloat("TorchVolume", volume);
    }
    /// <summary>
    /// Configura el volumen de los sonidos de jugador
    /// </summary>
    /// <param name="volume"></param>
    public void SetSoundPlayer(float volume)
    {
        PlayerPrefs.SetFloat("PlayerVolume", volume);
        audioMixer.SetFloat("PlayerVolume", volume);
    }
    /// <summary>
    /// Configura el volumen de los sonidos de boss
    /// </summary>
    /// <param name="volume"></param>
    public void SetSoundBoss(float volume)
    {
        PlayerPrefs.SetFloat("BossVolume", volume);
        audioMixer.SetFloat("BossVolume", volume);
    }
}
