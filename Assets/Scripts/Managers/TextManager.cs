using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TextManager : MonoBehaviour
{
    [Header("Texto")]
    public GameObject textPanel;
    public Text text;
    public Text character;

    public static TextManager instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        textPanel.SetActive(false);
    }

    #region METHODS
    /// <summary>
    /// Muestra el texto por pantalla
    /// </summary>
    /// <param name="message"></param>
    /// <param name="textColor"></param>
    public void DisplayMessage(string message, Color textColor, string characterName)
    {
        character.text = characterName;
        text.text = message;
        text.color = textColor;

        textPanel.SetActive(true);
    }
    /// <summary>
    /// Oculta el texto y el panel de mensajes
    /// </summary>
    public void HideMessage()
    {
        // Se vacia el texto
        text.text = "";
        // Se desactiva el panel
        textPanel.SetActive(false);
    }
    #endregion
}
