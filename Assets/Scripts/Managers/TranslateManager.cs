using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Realizar lectura del XML
using System.Xml;
using UnityEngine.UI;
public class TranslateManager : MonoBehaviour
{
    #region VARIABLES
    // Idioma cargado por defecto, en caso de no encontrar idioma del sistema operativo
    public string defaultLanguage = "Spanish";
    // Listado de literales de texto junto con su c�digo
    public Dictionary<string, string> Strings;

    public Text firstLanguageText;
    public List<string> names;
    private int optionNumber = 0;
    public int maxValues = 1;

    public TextAsset[] languagesInGame;

    public List<Text> textsFromGame;
    // Singleton
    public static TranslateManager instance;
    #endregion
    #region UNITY EVENTS
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    void Start()
    {
        // Recupera el nombre del idioma actual del sistema
        string language = Application.systemLanguage.ToString();
        // De manera expl�cita se trata de recoger el fichero XML de idioma como TextAsset, de la carpeta RESOURCES
        TextAsset textAsset = (TextAsset)Resources.Load("Lang/" + language, typeof(TextAsset));
    }

    private void Update()
    {
        // Variable de XmlDocument
        XmlDocument xml = new XmlDocument();
        // Se carga el xml desde el fichero de texto plano
        ChangeLanguage(xml);
    }
    #endregion
    #region METHODS
    public void ChangeLanguageOptions(int sum)
    {
        optionNumber += sum;

        if (optionNumber < 0)
        {
            optionNumber = maxValues;
            firstLanguageText.text = names[optionNumber];
        }
        else if (optionNumber > maxValues)
        {
            optionNumber = 0;
            firstLanguageText.text = names[optionNumber];
        }
        else
        {
            firstLanguageText.text = names[optionNumber];
        }

    }
    public void ChangeLanguage(XmlDocument xml)
    {
        for (int i = 0; i < names.Count; i++)
        {
            if (optionNumber == i)
            {
                xml.LoadXml(languagesInGame[optionNumber].text);
                SetLanguage(xml);
                TranslationWithCodes();
            }

        }

    }
    /// <summary>
    /// Revisa los TEXT de los BUTTON que est�n asociados a �stos, y modifica su idioma conforme a eso
    /// </summary>
    private void TranslationWithCodes()
    {
        foreach (Text textFromGame in textsFromGame)
        {
            switch (textFromGame.name)
            {
                case "PushButton":
                    textFromGame.text = GetString("PushButton");
                    break;
                case "StartGame":
                    textFromGame.text = GetString("StartGame");
                    break;
                case "Instructions":
                    textFromGame.text = GetString("Instructions");
                    break;
                case "Options":
                    textFromGame.text = GetString("Options");
                    break;
                case "Credits":
                    textFromGame.text = GetString("Credits");
                    break;
                case "Exit":
                    textFromGame.text = GetString("Exit");
                    break;
                case "Back":
                    textFromGame.text = GetString("Back");
                    break;
                case "Back2":
                    textFromGame.text = GetString("Back");
                    break;
                case "Description":
                    textFromGame.text = GetString("Description");
                    break;
                case "HowToPlay":
                    textFromGame.text = GetString("HowToPlay");
                    break;
                case "MusicVolume":
                    textFromGame.text = GetString("MusicVolume");
                    break;
                case "SoundVolume":
                    textFromGame.text = GetString("SoundVolume");
                    break;
                case "GameSounds":
                    textFromGame.text = GetString("GameSounds");
                    break;
                case "Sounds":
                    textFromGame.text = GetString("Sounds");
                    break;
                case "TorchVolume":
                    textFromGame.text = GetString("SoundsTorch");
                    break;
                case "Language":
                    textFromGame.text = GetString("Language");
                    break;
                case "Fullscreen":
                    textFromGame.text = GetString("Fullscreen");
                    break;
                case "Colorblind":
                    textFromGame.text = GetString("Colorblind");
                    break;
                case "SaveGame":
                    textFromGame.text = GetString("SaveGame");
                    break;
                case "DeleteGame":
                    textFromGame.text = GetString("DeleteGame");
                    break;
                case "GamepadControls":
                    textFromGame.text = GetString("GamepadControls");
                    break;
                case "PCControls":
                    textFromGame.text = GetString("PCControls");
                    break;
                case "PauseMenu":
                    textFromGame.text = GetString("PauseMenu");
                    break;
                case "GamepadControlSettings":
                    textFromGame.text = GetString("GamepadControlSettings");
                    break;
                case "PCControlSettings":
                    textFromGame.text = GetString("PCControlSettings");
                    break;
                case "CheckControls":
                    textFromGame.text = GetString("CheckControls");
                    break;
                case "Shop":
                    textFromGame.text = GetString("Shop");
                    break;
                case "ItemList":
                    textFromGame.text = GetString("ItemList");
                    break;
                case "Item":
                    textFromGame.text = GetString("Item");
                    break;
                case "Price":
                    textFromGame.text = GetString("Price");
                    break;
                case "Owned":
                    textFromGame.text = GetString("Owned");
                    break;
                case "Arrow":
                    textFromGame.text = GetString("Arrow");
                    break;
                case "Potion":
                    textFromGame.text = GetString("Potion");
                    break;
                case "Blue Gem":
                    textFromGame.text = GetString("BlueGem");
                    break;
                case "YouWantToBuyIt":
                    textFromGame.text = GetString("YouWantToBuyIt");
                    break;
                case "Yes":
                    textFromGame.text = GetString("Yes");
                    break;
                case "No":
                    textFromGame.text = GetString("No");
                    break;
                case "Movement":
                    textFromGame.text = GetString("Movement");
                    break;
                case "MeleeAttack":
                    textFromGame.text = GetString("MeleeAttack");
                    break;
                case "RangeAttack":
                    textFromGame.text = GetString("RangeAttack");
                    break;
                case "LockEnemy":
                    textFromGame.text = GetString("LockEnemy");
                    break;
                case "Interaction":
                    textFromGame.text = GetString("Interaction");
                    break;
                case "Run":
                    textFromGame.text = GetString("Run");
                    break;
                case "Dash":
                    textFromGame.text = GetString("Dash");
                    break;
                case "Inventory":
                    textFromGame.text = GetString("Inventory");
                    break;
                case "HealPlayer":
                    textFromGame.text = GetString("HealPlayer");
                    break;
                case "LeftJoystick":
                    textFromGame.text = GetString("LeftJoystick");
                    break;
                case "HoldLeftJoystick":
                    textFromGame.text = GetString("HoldLeftJoystick");
                    break;
                case "LeftMouseClick":
                    textFromGame.text = GetString("LeftMouseClick");
                    break;
                case "RightMouseClick":
                    textFromGame.text = GetString("RightMouseClick");
                    break;
                case "Spacebar":
                    textFromGame.text = GetString("Spacebar");
                    break;
                case "Galapua":
                    textFromGame.text = GetString("Galapua");
                    break;
                case "FireCamp":
                    textFromGame.text = GetString("FireCamp");
                    break;
                case "Castle":
                    textFromGame.text = GetString("Castle");
                    break;
                case "YouWantToGoGalapua":
                    textFromGame.text = GetString("YouWantToGoGalapua");
                    break;
                case "YouWantToGoFireCamp":
                    textFromGame.text = GetString("YouWantToGoFireCamp");
                    break;
                case "YouWantToGoLohokCastle":
                    textFromGame.text = GetString("YouWantToGoLohokCastle");
                    break;
                case "Resume":
                    textFromGame.text = GetString("Resume");
                    break;
                case "MainMenu":
                    textFromGame.text = GetString("MainMenu");
                    break;
                case "DeathMenu":
                    textFromGame.text = GetString("GameOver");
                    break;
                case "Restart":
                    textFromGame.text = GetString("Restart");
                    break;
                case "PlayerVolume":
                    textFromGame.text = GetString("PlayerVolume");
                    break;
                case "BossVolume":
                    textFromGame.text = GetString("BossVolume");
                    break;
                default:
                    textFromGame.text = "#LANGERROR#";
                    break;
            }
        }
    }

    /// <summary>
    /// Carga el idioma seleccionado del XML
    /// </summary>
    /// <param name="xml"></param>
    public void SetLanguage(XmlDocument xml)
    {
        // Inicializa el diccionario dej�ndolo vac�o
        Strings = new Dictionary<string, string>();
        // Devuelve los elementos que se encuentran entre la etiqueta "lang" en el XML de cada idioma
        XmlElement element = xml.DocumentElement["lang"];
        // Lee cada l�nea dentro de "lang"
        IEnumerator elemEnum = element.GetEnumerator();
        // Obtiene el siguiente elemento despreciando los valores anteriores, si no hay m�s dentro de "lang", termina.
        while (elemEnum.MoveNext())
        {
            // Se recupera el literal actual de texto
            XmlElement xmlItem = (XmlElement)elemEnum.Current;
            // Se recupera el valor atributo name
            string xmlAttribute = xmlItem.GetAttribute("name");
            // Recoge lo que se contiene ENTRE/DENTRO de la etiqueta -> <lang>( ESTO DE AQU� )</lang> 
            Strings.Add(xmlAttribute, xmlItem.InnerText);
        }

    }
    /// <summary>
    /// Recupera un literal de texto mediante el �ndice
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public string GetString(string name)
    {
        // Si no existe el �ndice en el diccionario
        if (!Strings.ContainsKey(name))
        {
            //Debug.LogWarning("La cadena no existe" + name);
            // Si no se encuentra el string, se escribe una cadena vac�a
            return "#LANG_ERROR#";
        }
        // Si existe, devolvemos el texto asociado
        return Strings[name];
    }
    #endregion
}
