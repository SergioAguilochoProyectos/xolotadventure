using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveVillager : MonoBehaviour
{
    public List<Transform> wayPoints;
    public int nextTarget;
    public GameObject player;

    public NavMeshAgent villagerAgent;
    public Animator villagerAnim;

    private Interactable interactable;

    //public bool isPlayingReaction;

    public float speed = 2f;

    public static MoveVillager instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }
    private void Start()
    {
        interactable = GetComponent<Interactable>();

        if (DataManager.instance.data.missionCounter <= 12 && gameObject.name == "Kid")
        {
            gameObject.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (DataManager.instance.data.missionCounter <= 12 && gameObject.name == "Kid")
        {
            gameObject.SetActive(false);
        }

        if (!interactable.isPlayingReaction)
        {
            villagerAnim.SetBool("isWalking", true);
            // Se recupera el siguiente waypoint y se establece como destino
            villagerAgent.destination = wayPoints[nextTarget].position;
            // Se inicia el movimiento hacia el destino
            villagerAgent.isStopped = false;
            // Se asigna la velocidad de patrulla
            villagerAgent.speed = speed;

            if (villagerAgent.remainingDistance <= villagerAgent.stoppingDistance &&
                !villagerAgent.pathPending)
            {
                villagerAnim.SetBool("isWalking", false);
                nextTarget = Random.Range(0, wayPoints.Count);
            }

        }
        else
        {
            villagerAgent.isStopped = true;
            villagerAgent.speed = 0f;

            villagerAnim.SetBool("isWalking", false);

            transform.LookAt(new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z));
        }

    }

}
