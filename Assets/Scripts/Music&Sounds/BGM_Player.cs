using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGM_Player : MonoBehaviour
{
    public static BGM_Player playMusic; //SINGLETON

    public AudioSource backgroundMusic;

    void Awake()
    {
        //MUSICA DE FONDO

        if (playMusic == null)
        {
            DontDestroyOnLoad(gameObject);
            playMusic = this;
        }
        else if (playMusic != this)
        {
            Destroy(gameObject);
        }
    }
    /// <summary>
    /// Comienza o reinicia la m�sica de fondo
    /// </summary>
    /// <param name="restartMusic"></param>
    public void StartBGM(bool restartMusic)
    {
        if (!backgroundMusic.isPlaying || restartMusic) //Si est� detenida o por reiniciarse la m�sica
        {
            backgroundMusic.Play(); //Se reproduce desde el principio
        }
    }
    /// <summary>
    /// Para la m�sica de fondo
    /// </summary>
    public void StopBGM()
    {
        backgroundMusic.Stop();
    }
}
