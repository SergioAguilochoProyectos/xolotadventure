using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusicHandler : MonoBehaviour
{
    public enum Mode { Playing, Restart, Stop };

    public Mode mode;
    void Start()
    {
        if (mode == Mode.Playing)
        {
            BGM_Player.playMusic.StartBGM(false); //Sigue reproduciendo al cargar una escena
        }
        else if (mode == Mode.Restart)
        {
            BGM_Player.playMusic.StartBGM(true); //Se reinicia al cargar una escena
        }
        else if (mode == Mode.Stop)
        {
            BGM_Player.playMusic.StopBGM(); //Se detiene la m�sica
        }
    }
}
