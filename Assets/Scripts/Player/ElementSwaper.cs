using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ElementSwaper : MonoBehaviour
{
    #region VARIABLES

    public GameObject[] element;

    public bool nullElementBool = true;
    public bool jadeElementBool = false;
    public bool fireElementBool = false;
    public bool waterElementBool = false;

    public int elementPos;

    private PlayerAttack playerAttack;

    #endregion

    
    #region UNITY
// Start is called before the first frame update
    void Start()
    {
        nullElementBool = true;

        elementPos = 0;

        playerAttack = GetComponent<PlayerAttack>();
    }

    // Update is called once per frame
    void Update()
    {
        element[elementPos].SetActive(true);

        if(elementPos == 0)
        {
            element[1].SetActive(false);
            element[2].SetActive(false);
            element[3].SetActive(false);
            playerAttack.jadeGem = false;
            playerAttack.fireGem = false;
            playerAttack.waterGem = false;            
        }
        else if(elementPos == 1)
        {
            element[0].SetActive(false);
            element[2].SetActive(false);
            element[3].SetActive(false);
            playerAttack.jadeGem = true;
            playerAttack.fireGem = false;
            playerAttack.waterGem = false;
        }
        else if(elementPos == 2)
        {
            element[0].SetActive(false);
            element[1].SetActive(false);
            element[3].SetActive(false);
            playerAttack.jadeGem = false;
            playerAttack.fireGem = true;
            playerAttack.waterGem = false;
        }
        else if(elementPos == 3)
        {
            element[0].SetActive(false);
            element[1].SetActive(false);
            element[2].SetActive(false);
            playerAttack.jadeGem = false;
            playerAttack.fireGem = false;
            playerAttack.waterGem = true;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            if(elementPos >= 3)
            {
                Debug.Log("Element 0");
                elementPos = 0;
            }
            else
            {
                Debug.Log("Element ++");
                elementPos++;
            }
        }
    
    }

    #endregion

    #region METHODS



    #endregion
}
