using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerAttack : MonoBehaviour
{
    [Header("Melee")]
    public int baseAttackDamage = 15;
    public int currentAttackDamage;
    public bool canAttack;
    private bool keepAttacking;
    public bool isAttacking;
    private Vector3 attackMovement;
    public float attackMovementMultiplier;
    public float attackTimer = 0.5f;
    public float strongAttackTime;
    public float attackStamina = 10;

    [Header("Distance Attack")]
    public bool canShoot;
    public int maxArrowLoad;
    public int currentArrowLoad;
    public float arrowForce;
    public int arrowDamage = 10;
    public float coolDown = 2f;
    public PlayerBullet nullArrowPrefab;
    public PlayerBullet fireArrowPrefab;
    public PlayerBullet jadeArrowPrefab;
    public PlayerBullet waterArrowPrefab;
    private Rigidbody rb;
    public float rangeCoolDownTimer;
    public Transform arrowSpawn;
    private AudioSource audioSource;
    public Transform playerSectionContainer;
    public float shootStamina = 5;
    public float shootForce;
    public Text currentArrowLoadText;
    public Text maxArrowLoadText;

    public Animator animator;
    public Animator hUDAnimator;
    public PlayerStamina playerStamina;
    public float staminaCost = 10f;
    private PlayerController playerController;
    private PlayerStamina stamina;

    private Coroutine combo;

    public Collider swordCollider;
    public AudioClip shotClip;
    public AudioClip attack;

    [Header("Gems")]
    public bool fireGem;
    public bool jadeGem;
    public bool waterGem;
    public GameObject fireSword;
    public GameObject jadeSword;
    public GameObject waterSword;
    public GameObject fireCrossbow;
    public GameObject jadeCrossbow;
    public GameObject waterCrossbow;
    public ParticleSystem basicParticleTrail;
    public ParticleSystem basicParticles;
    public ParticleSystem fireParticleTrail;
    public ParticleSystem firstFireParticles;
    public ParticleSystem secondFireParticles;
    public ParticleSystem jadeParticleTrail;
    public ParticleSystem jadeParticles;
    public ParticleSystem waterParticleTrail;
    public ParticleSystem firstWaterParticles;
    public ParticleSystem secondWaterParticles;

    private EnemyLockOn lockOn;

    public static PlayerAttack instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody>();
        playerController = GetComponent<PlayerController>();
        stamina = GetComponent<PlayerStamina>();
        lockOn = GetComponent<EnemyLockOn>();
        canAttack = true;
        canShoot = true;
        swordCollider.enabled = false;
        fireSword.SetActive(false);
        jadeSword.SetActive(false);
        waterSword.SetActive(false);

        maxArrowLoad = DataManager.instance.data.maxArrow;

        isAttacking = false;
    }

    // Update is called once per frame
    void Update()
    {
        rangeCoolDownTimer -= Time.deltaTime;

        rangeCoolDownTimer = Mathf.Clamp(rangeCoolDownTimer, -1, coolDown);

        currentArrowLoadText.text = currentArrowLoad.ToString();
        maxArrowLoadText.text = maxArrowLoad.ToString();

        for (int i = 0; i < DataManager.instance.data.inventory.Length; i++)
        {
            if (DataManager.instance.data.inventory[i].name == "Arrow")
            {
                currentArrowLoad = DataManager.instance.data.inventory[i].amount;
            }
        }

        if (!keepAttacking) isAttacking = false;

        if(isAttacking)
        {
            attackMovement = transform.forward;
            playerController.movement.Set(attackMovement.x, 0, attackMovement.z);
            playerController.player.Move(attackMovement * attackMovementMultiplier * Time.deltaTime);
        }

        if (currentArrowLoad > maxArrowLoad) currentArrowLoad = maxArrowLoad;

        if (stamina.currentStamina > 0) hUDAnimator.enabled = false;

        if (!CheatsManager.instance.isFullDmg)
        {
            CheckGem();
        }

        if (SceneManager.GetActiveScene().name == "FireCamp 1")
        {
            canAttack = true;
        }

        canAttack = stamina.currentStamina > 0 ? true : false;

        if ((Input.GetButtonDown("Fire1") || Input.GetAxis("Fire1") == 1) && canAttack)
        {
            if (stamina.currentStamina <= 0)
            {
                Debug.Log("No stamina");
                hUDAnimator.SetTrigger("NoStamina");
                animator.SetBool("KeepAttacking", false);
                return;
            }
            if (combo != null)
            {
                StopCoroutine(combo);
            }

            if(!keepAttacking)
            {
                animator.SetTrigger("FirstAttack");
                combo = StartCoroutine(WaitForCombo(attackTimer));
            }
            else
            {
                combo = StartCoroutine(WaitForCombo(attackTimer));
            }

        }

        for (int i = 0; i < DataManager.instance.data.inventory.Length; i++)
        {
            if (DataManager.instance.data.inventory[i].name == "Arrow" && DataManager.instance.data.inventory[i].amount > 0)
            {
                if ((Input.GetButtonDown("Fire2") || Input.GetAxis("Fire2") == 1) && lockOn.enemyLocked && currentArrowLoad > 0 && canShoot)
                {
                    if (stamina.currentStamina < 10)
                    {
                        hUDAnimator.SetTrigger("NoStamina");
                        return;
                    }

                    if (rangeCoolDownTimer > 0)
                    {
                        return;
                    }

                    playerController.canMove = false;

                    //reiniciamos el contador de tiempo de cooldown
                    rangeCoolDownTimer = coolDown;

                    audioSource.PlayOneShot(shotClip);
                    animator.SetTrigger("Point");
                    stamina.ConsumeStamina(shootStamina);
                    currentArrowLoad--;

                    InventoryManager.instance.RemoveItemInventory("Arrow");
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetAxis("NullEffect") == -1)
        {
            waterGem = false;
            fireGem = false;
            jadeGem = false;
        }

        for (int i = 0; i < DataManager.instance.data.inventory.Length; i++)
        {
            if (DataManager.instance.data.inventory[i].name == "Jade")
            {
                if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetAxis("JadeGemEffect") == -1)
                {
                    waterGem = false;
                    fireGem = false;
                    jadeGem = true;
                }
            }
            if (DataManager.instance.data.inventory[i].name == "Celeste")
            {
                if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetAxis("WaterGemEffect") == 1)
                {
                    waterGem = true;
                    fireGem = false;
                    jadeGem = false;
                }
            }
            if (DataManager.instance.data.inventory[i].name == "Infernal")
            {
                if (Input.GetKeyDown(KeyCode.Alpha4) || Input.GetAxis("FireGemEffect") == 1)
                {
                    waterGem = false;
                    fireGem = true;
                    jadeGem = false;
                }
            }
        }
    }

    /// <summary>
    /// Identifica si seguimos atacando antes de volver al idle
    /// </summary>
    /// <returns></returns>
    private IEnumerator WaitForCombo(float timer)
    {
        float tempTimer = timer;

        playerController.canMove = false;

        animator.SetBool("KeepAttacking", true);

        keepAttacking = true;

        while(tempTimer > 0)
        {
            tempTimer -= Time.deltaTime;
            yield return null;
        }
        animator.SetBool("KeepAttacking", false);

        keepAttacking = false;

        playerController.canMove = true;
        
    }

    /// <summary>
    /// Comprueba y activa el elemento de la gema que esté activa
    /// </summary>
    public void CheckGem()
    {
        if (fireGem)
        {
            currentAttackDamage = baseAttackDamage * 2;
            jadeSword.SetActive(false);
            fireSword.SetActive(true);
            waterSword.SetActive(false);
            jadeCrossbow.SetActive(false);
            fireCrossbow.SetActive(true);
            waterCrossbow.SetActive(false);
        }
        else if (jadeGem)
        {
            currentAttackDamage = baseAttackDamage * 2;
            fireSword.SetActive(false);
            waterSword.SetActive(false);
            jadeSword.SetActive(true);
            jadeCrossbow.SetActive(true);
            fireCrossbow.SetActive(false);
            waterCrossbow.SetActive(false);
        }
        else if (waterGem)
        {
            currentAttackDamage = baseAttackDamage * 2;
            fireSword.SetActive(false);
            jadeSword.SetActive(false);
            waterSword.SetActive(true);
            jadeCrossbow.SetActive(false);
            fireCrossbow.SetActive(false);
            waterCrossbow.SetActive(true);
        }
        else
        {
            currentAttackDamage = baseAttackDamage;
            fireSword.SetActive(false);
            jadeSword.SetActive(false);
            waterSword.SetActive(false);
            jadeCrossbow.SetActive(false);
            fireCrossbow.SetActive(false);
            waterCrossbow.SetActive(false);
        }
    }

    
    /// <summary>
    /// Permitimos el movimiento
    /// </summary>
    private void AllowMovement()
    {
        playerController.canMove = true;
    }
    
    /// <summary>
    /// Permitimos el movimiento
    /// </summary>
    private void ForbidMovement()
    {
        playerController.canMove = false;
    }

    /// <summary>
    /// Disparamos las flechas
    /// </summary>
    private void Shoot()
    {
        if(fireGem)
        {
            PlayerBullet rbBullet = Instantiate(fireArrowPrefab, arrowSpawn.position, transform.rotation, playerSectionContainer);
            rbBullet.rigidbody.AddRelativeForce(new Vector3(0f, 0f, 700f) * shootForce);
        }
        else if(waterGem)
        {
            PlayerBullet rbBullet = Instantiate(waterArrowPrefab, arrowSpawn.position, transform.rotation, playerSectionContainer);
            rbBullet.rigidbody.AddRelativeForce(new Vector3(0f, 0f, 700f) * shootForce);
        }
        else if(jadeGem)
        {
            PlayerBullet rbBullet = Instantiate(jadeArrowPrefab, arrowSpawn.position, transform.rotation, playerSectionContainer);
            rbBullet.rigidbody.AddRelativeForce(new Vector3(0f, 0f, 700f) * shootForce);
        }
        else
        {
            PlayerBullet rbBullet = Instantiate(nullArrowPrefab, arrowSpawn.position, transform.rotation, playerSectionContainer);
            rbBullet.rigidbody.AddRelativeForce(new Vector3(0f, 0f, 700f) * shootForce);
        }
        
    }

    /// <summary>
    /// Ejecuta el clip de audio
    /// </summary>
    private void AudioPlay()
    {
        audioSource.PlayOneShot(attack);
    }

    /// <summary>
    /// Activa el collider de la espada
    /// </summary>
    private void ActivateCollider()
    {
        swordCollider.enabled = true;
    }
    
    /// <summary>
    /// Impide el disparo
    /// </summary>
    private void ForbidShooting()
    {
        canShoot = false;
    }

    /// <summary>
    /// Permite el disparo
    /// </summary>
    private void AllowShooting()
    {
        canShoot = true;
    }

    /// <summary>
    /// Desactiva el collider de la espada
    /// </summary>
    private void DeactivateCollider()
    {
        swordCollider.enabled = false;
    }

    /// <summary>
    /// Activa las partículas del trail de ataque de derecha a izquierda
    /// </summary>
    private void ActivateFirstAttackTrail()
    {
        
        if(fireGem)
        {
            firstFireParticles.Play();
            fireParticleTrail.Play();
        }
        else if(jadeGem)
        {
            jadeParticles.Play();
            jadeParticleTrail.Play();
        }
        else if(waterGem)
        {
            firstWaterParticles.Play();
            waterParticleTrail.Play();
        }
        else
        {
            basicParticles.Play();
            basicParticleTrail.Play();
        }
    }


    /// <summary>
    /// Activa las partículas del trail de ataque de izquierda a derecha
    /// </summary>
    private void ActivateSecondAttackTrail()
    {
        if(fireGem)
        {
            secondFireParticles.Play();
            fireParticleTrail.Play();
        }
        else if(jadeGem)
        {
            jadeParticles.Play();
            jadeParticleTrail.Play();
        }
        else if(waterGem)
        {
            secondWaterParticles.Play();
            waterParticleTrail.Play();
        }
        else
        {
            basicParticles.Play();
            basicParticleTrail.Play();
        }
    }
    
    /// <summary>
    /// Desactiva las partículas del trail de ataque
    /// </summary>
    private void DeactivateAttackTrail()
    {
        basicParticles.Stop();
        basicParticleTrail.Stop();
        firstFireParticles.Stop();
        secondFireParticles.Stop();
        fireParticleTrail.Stop();
        jadeParticles.Stop();
        jadeParticleTrail.Stop();
        firstWaterParticles.Stop();
        secondWaterParticles.Stop();
        waterParticleTrail.Stop();
    }

    /// <summary>
    /// Resetea el ataque en el animator
    /// </summary>
    public void StopAttacking()
    {
        animator.SetBool("KeepAttacking", false);

        keepAttacking = false;
    }

    /// <summary>
    /// Mueve al jugador hacia delante en los ataques
    /// </summary>
    public void AttackMoveToggle()
    {
        if(!isAttacking)
        {
            isAttacking = true;
        }
        else
        {
            isAttacking = false;
        }
    }

}
