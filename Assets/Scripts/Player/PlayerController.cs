using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{
    #region VARIABLES

    [Header("Movement")]
    public float walkSpeed;
    public float runSpeed;
    public float currentSpeed;
    public float moveHorizontal, moveVertical;
    public float verticalV = -10;
    public bool isRunning;
    public bool canMove;
    public Vector3 movement = new Vector3();

    [Header("Rotation")]
    [HideInInspector] public float rotationHorizontal, rotationVertical;
    public float sensibility = 2f;
    public bool invertView;
    public Quaternion cameraRotation;
    public Quaternion currentCameraRotation;
    public Camera mainCamera;
    public float rotationSpeed;
    public Transform playerObject;

    [Header("Interaction")]
    public LayerMask interactableLayer;
    public bool canInteract;
    public Interactable interactable;
    public CanvasGroup interactableMessage;


    public CharacterController player;
    [HideInInspector] public Animator anim;
    public AudioSource audioSource;
    PlayerStamina stamina;
    private EnemyLockOn locking;
    private CameraHandler cameraHandler;

    public static PlayerController instance;

    public bool saveState;
    int counter = 0;

    #endregion

    #region UNITY EVENTS

    private void Awake()
    {
        cameraHandler = CameraHandler.instance;

        if (instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        player = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
        stamina = GetComponent<PlayerStamina>();
        locking = GetComponent<EnemyLockOn>();
        canMove = false;

        
        if (DataManager.instance.data.missions[5].isDone)
        {
            canMove = true;
        }
        

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        
  
        if (DataManager.instance.data.entrancePosition != "")
        {
            // SaveState al iniciar la partida debe estar en TRUE y desactivarse al segundo.
            saveState = true;
        }
        else
        {
            saveState = false;
        }
        StartCoroutine(DelaySaveStateBool());
               
    }

    private void FixedUpdate()
    {
        
        GameObject startPosition = GameObject.Find(DataManager.instance.data.entrancePosition);
           

        // Si ha podido recuperar un start position
        if (startPosition)
        {
            if(saveState == true)
            {
                

                transform.position = startPosition.transform.position;

                transform.rotation = startPosition.transform.rotation;
            }

        }     
        
        
        float delta = Time.deltaTime;

        ApplyGravity();
        Controls();

        if (cameraHandler != null)
        {
            cameraHandler.FollowTarget(delta);
            cameraHandler.HandleCameraRotation(delta, -rotationHorizontal, -rotationVertical);
        }
    }

    // Update is called once per frame
    void Update()
    {
        currentCameraRotation = cameraRotation;

        if (stamina.currentStamina <= 0)
        {
            anim.SetBool("Sprint", false);
            isRunning = false;
        }

        if(isRunning)
        {
            stamina.ConsumeStamina(0.5f);
        }

        if (canMove)
        {
            Movement();       
        }
        if (locking == null)
        {
            return;
        }
        else if (locking.enemyLocked) transform.LookAt(locking.enemyTarget);

        if (canInteract)
        {
            interactableMessage.alpha = 1;
        }
        else
        {
            interactableMessage.alpha = 0;
        }

        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Interactable"))
        {
            interactable = other.gameObject.GetComponent<Interactable>();

            canInteract = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Interactable"))
        {
            canInteract = false;

            TextManager.instance.HideMessage();
        }
    }

    #endregion

    #region METHODS

    /// <summary>
    /// Recoge los inputs
    /// </summary>
    private void Controls()
    {
        moveHorizontal = Input.GetAxisRaw("Horizontal");
        moveVertical = Input.GetAxisRaw("Vertical");

        rotationHorizontal = Input.GetAxisRaw("Mouse X") * sensibility;
        rotationVertical += Input.GetAxisRaw("Mouse Y") * sensibility * (invertView ? 1 : -1);
        rotationVertical = Mathf.Clamp(rotationVertical, -80f, 80f);


        if (Input.GetButtonDown("Run") && moveVertical > 0)
        {
            if (stamina.currentStamina > 0)
            {
                anim.SetBool("Sprint", true);
                isRunning = true;
            }


        }

        if (Input.GetButtonUp("Run") || moveVertical <= 0)
        {
            anim.SetBool("Sprint", false);
            isRunning = false;
        }

        if (Input.GetButtonDown("Interact") && canInteract)
        {
            InteractWith();
        }
    }

    /// <summary>
    /// Realiza el movimiento en base a los controles recogidos
    /// </summary>
    private void Movement()
    {
        if(!isRunning)
        {
            currentSpeed = walkSpeed;
        }
        else
        {
            currentSpeed = runSpeed;
        }

        moveVertical = Mathf.Clamp(moveVertical, -walkSpeed, walkSpeed);

        movement.Set(moveHorizontal, 0, moveVertical);
        movement = movement.normalized;
        movement = transform.rotation * movement;

        player.Move(movement * currentSpeed * Time.deltaTime);
        anim.SetFloat("Forward", moveVertical);
        anim.SetFloat("Right", moveHorizontal * 0.5f);

        cameraRotation = new Quaternion(0f, mainCamera.transform.rotation.y, 0f, mainCamera.transform.rotation.w);
        transform.rotation = cameraRotation;
    }

    /// <summary>
    /// Aplica la gravedad al player
    /// </summary>
    public void ApplyGravity()
    {
        if (player.isGrounded) return;

        player.Move(Vector3.up * verticalV * Time.deltaTime);
    }

    /// <summary>
    /// Realiza las funciones de interacción
    /// </summary>
    public void InteractWith()
    {
        if (interactable != null)
        {
            interactable.Interact(this);
        }

        interactable = null;
    }

    /// <summary>
    /// Cambia el bool SaveState a False para evitar que cambie el punto de reaparición de jugador al anterior
    /// </summary>
    /// <returns></returns>
    public IEnumerator DelaySaveStateBool()
    {
        saveState = true;
        yield return new WaitForSeconds(0.05f);
        saveState = false;
    }

    #endregion
}
