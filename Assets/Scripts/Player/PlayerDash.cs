using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDash : MonoBehaviour
{
    PlayerStamina stamina;
    PlayerController playerController;
    public AudioClip dashSound;

    public float dashSpeed;
    public float dashTime;
    public float staminaCost = 10;

    public float dashCoolDown;
    public float coolDownTimer;

    public bool canDash;

    public Animator anim;

    public TrailRenderer leftRenderer;
    public TrailRenderer rightRenderer;
    public TrailRenderer neckRenderer;

    public static PlayerDash instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        canDash = false;
        playerController = GetComponent<PlayerController>();
        stamina = GetComponent<PlayerStamina>();

        leftRenderer.enabled = false;
        rightRenderer.enabled = false;
        neckRenderer.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        if(coolDownTimer > 0)
        {
            canDash = false;
            coolDownTimer -= Time.deltaTime;
        }
        else
        {
            //La misi�n de matar a todos los enemigos, si ya ha matado a alguno
            //if(DataManager.instance.data.statistics[2].value != 0)
            if(DataManager.instance.data.missions[1].isDone)
            canDash = true;
        }

            if (Input.GetButtonDown("Dash") && canDash)
            {
                if ((stamina.currentStamina < 10) || playerController.movement == new Vector3(0, 0, 0))
                {
                PlayerAttack.instance.hUDAnimator.SetTrigger("NoStamina");
                    return;
                }
                
                StartCoroutine(Dash());
                stamina.ConsumeStamina(staminaCost);

                if (playerController.moveVertical > 0 && playerController.moveHorizontal == 0)
                {
                    anim.SetTrigger("Dash");
                }
                else if (playerController.moveVertical < 0 && playerController.moveHorizontal == 0)
                {
                    anim.SetTrigger("BackDash");
                }
                else if(playerController.moveVertical == 0 && playerController.moveHorizontal >= 0)
                {
                    anim.SetTrigger("RightDash");
                }
                else if(playerController.moveVertical == 0 && playerController.moveHorizontal <= 0)
                {
                    anim.SetTrigger("LeftDash");
                }
            }
    }

    /// <summary>
    /// Realiza las acciones de un desplazamiento r�pido
    /// </summary>
    /// <returns></returns>
    IEnumerator Dash()
    {
        yield return new WaitForSeconds(0.125f);

        coolDownTimer = dashCoolDown;

        float startTime = Time.time;

        Debug.Log("Dash");

        playerController.audioSource.PlayOneShot(dashSound);

        while(Time.time < startTime + dashTime)
        {
            playerController.player.Move(playerController.movement * dashSpeed * Time.deltaTime);

            yield return null;
        }
    }

    /// <summary>
    /// Activa los trails renderers del dash
    /// </summary>
    private void ActivateDashTrails()
    {
        leftRenderer.enabled = true;
        rightRenderer.enabled = true;
        neckRenderer.enabled = true;
    }
    
    /// <summary>
    /// Activa los trails renderers del dash
    /// </summary>
    private void DeactivateDashTrails()
    {
        leftRenderer.enabled = false;
        rightRenderer.enabled = false;
        neckRenderer.enabled = false;
    }
}
