using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VFX;

public class PlayerHealth : MonoBehaviour, IDamageable
{
    #region Variables
    [Tooltip("La cantidad de salud m�xima.")]
    public int maxHealth;
    public Text potionAmount;
    private Animator anim;
    public PlayerAttack playerAttack;
    public PlayerController playerControl;
    public EnemyLockOn lockOn;
    public CapsuleCollider playerCollider;
    public GameManager gameManager;
    public Image[] hearts;
    public Sprite fullHeart;
    public Sprite emptyHeart;
    public int healAmount = 20;
    public AudioClip deathClip;
    public AudioClip hitClip;
    public AudioClip healClip;
    public bool canTakeDamage;
    //public ParticleSystem healParticles;
    public GameObject healVFX;
    public SkinnedMeshRenderer[] skinnedMeshRenderers;

    public bool canHeal = false;

    public static PlayerHealth instance;

    //public GameObject musicObject;

    #endregion

    #region Unity Events

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start() 
    {
        if (healVFX != null)
        {
            healVFX.gameObject.SetActive(false);
        }


        anim = GetComponent<Animator>();
        canTakeDamage = true;

        canHeal = false;
    }

    // Update is called once per frame
    void Update() 
    {
        UpdateHealthBar();

        DataManager.instance.data.playerHealth = Mathf.Clamp(DataManager.instance.data.playerHealth, 0, 100);

        for (int i = 0; i < DataManager.instance.data.inventory.Length; i++)
        {
            if (DataManager.instance.data.inventory[i].name == "Potion" && DataManager.instance.data.inventory[i].amount > 0)
            {
                potionAmount.text = DataManager.instance.data.potionAmount.ToString();

                if (canHeal)
                {
                    if (Input.GetButtonDown("Heal"))
                    {
                        InventoryManager.instance.RemoveItemInventory("Potion");

                        DataManager.instance.data.potionAmount--;

                        Heal();
                    }
                }
            }
        }

    }

    private void OnCollisionEnter(Collision collision) {
        switch (collision.collider.tag.ToString()) {
            case "Bullet":
                // TODO: Asignarle un attack damage al jugador que se a�adir� al valor de este takedamage
                
                
                    TakeDamage(10);
                
                break;
        }
    }
    #endregion

    #region Methods
    /// <summary>
    /// Calcula la salud actual
    /// </summary>
    /// <returns></returns>
    public float CalculateHealth() {
        return DataManager.instance.data.playerHealth / maxHealth;
    }

    /// <summary>
    /// Actualiza la barra de salud.
    /// </summary>
    public void UpdateHealthBar() {
        for (int i = 0; i < hearts.Length; i++) {
            if (i < DataManager.instance.data.playerHealth/10) {
                hearts[i].sprite = fullHeart;
                
            } else {
                hearts[i].sprite = emptyHeart;
            }
        }
        
        if (DataManager.instance.data.playerHealth > maxHealth)
        {
            DataManager.instance.data.playerHealth = maxHealth;
        }
    }

    public void Die() 
    {
        playerControl.enabled = false;
        playerAttack.enabled = false;
        playerCollider.enabled = false;
        lockOn.enabled = false;

        GameManager.instance.DeathMenu();
    }

    public void TakeDamage(int damageTaken) 
    {
        if (!canTakeDamage) return;

        DataManager.instance.data.playerHealth -= damageTaken;

        // Si la vida es menor que 100 se activa la opci�n de poder curarse, y SOLO la primera vez que se le hace da�o aparecer� el mensaje
        if (DataManager.instance.data.playerHealth < 100)
        {
            canHeal = true;
        }

        playerControl.anim.SetBool("KeepAttacking", false);

        DataManager.instance.Save();

        if (DataManager.instance.data.playerHealth <= 0)
        {
            anim.SetBool("Death", true);

            Die();

            if (DataManager.instance.data.playerHealth == 0)
            {
                StartCoroutine(AudioPlayer());

                StartCoroutine(GameManager.instance.AudioPlayer());

                //musicObject.SetActive(false);
            }

            return;
        }
        else
        {
            playerControl.audioSource.PlayOneShot(hitClip);
            anim.SetTrigger("Damage");
            StartCoroutine(TakeDamageCooldown(1f));
        }

    }

    private IEnumerator TakeDamageCooldown(float cooldown) {
        canTakeDamage = false;
        foreach (SkinnedMeshRenderer smr in skinnedMeshRenderers) {
            smr.material.SetColor("_BaseColor", Color.red);
        }
        yield return new WaitForSeconds(cooldown);
        foreach (SkinnedMeshRenderer smr in skinnedMeshRenderers) {
            smr.material.SetColor("_BaseColor", Color.white);
        }
        canTakeDamage = true;
    }

    /// <summary>
    /// Corrutina que hace que suene el sonido de muerte
    /// </summary>
    /// <returns></returns>
    private IEnumerator AudioPlayer()
    {
        playerControl.audioSource.PlayOneShot(deathClip);

        yield return new WaitForSeconds(1.3f);

        playerControl.audioSource.Stop();

    }

    
    /// <summary>
    /// Realiza la animaci�n y curaci�n
    /// </summary>
    /// <param name="damageHealed"></param>
    public void Heal()
    {
        anim.SetTrigger("Heal");

        DataManager.instance.Save();
        
        playerControl.canMove = false;

        DataManager.instance.data.playerHealth += healAmount;

        canHeal = false;

        if(playerAttack != null)
        {
            playerAttack.canShoot = false;
        }

    }

    /// <summary>
    /// Realiza las funciones del feedback de la cura
    /// </summary>
    private void HealFeedback()
    {
        healVFX.gameObject.SetActive(true);

        playerControl.audioSource.PlayOneShot(healClip);

        if (DataManager.instance.data.playerHealth == maxHealth) return;
    }

    /// <summary>
    /// Desactiva el VFX de la curaci�n
    /// </summary>
    private void HealFeedBackOff()
    {
        healVFX.gameObject.SetActive(false);
    }

    /// <summary>
    /// Permite recibir da�o
    /// </summary>
    private void AllowTakeDamage()
    {
        canTakeDamage = true;
    }
    
    /// <summary>
    /// Aunla la posibilidad de recibir da�o
    /// </summary>
    private void DenyTakeDamage()
    {
        canTakeDamage = false;
    }
    #endregion
}
