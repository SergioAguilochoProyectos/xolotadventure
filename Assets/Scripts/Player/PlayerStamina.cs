using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStamina : MonoBehaviour {

    #region Variables
    [Header("Stamina Variables")]
    [Tooltip("La resistencia m�xima")]
    public float maxStamina = 100;
    [Tooltip("La resistencia actual")]
    public float currentStamina;
    public float WaitTime = 30f;

    [Header("Stamina HUD")]
    [Tooltip("La imagen de relleno del contenedor de resistencia")]
    public Image staminaBar;

    public bool canRestore;
    public float staminaRestoration;
    public float restoreTimer;
    public float coolDown;

    #endregion

    #region Unity Events
    private void Start() {
        currentStamina = maxStamina;
    }

    private void Update() {

        restoreTimer -= Time.deltaTime;

        restoreTimer = Mathf.Clamp(restoreTimer, -1, coolDown);

        currentStamina = Mathf.Clamp(currentStamina, -20, maxStamina);

        staminaBar.fillAmount = (float)currentStamina / maxStamina;

        if (restoreTimer <= 0)
        {
            canRestore = true;
        }
        else canRestore = false;

        if(canRestore) RestoreStaminaOverTime();
    }
    #endregion

    #region Methods

    /// <summary>
    /// Consume la cantidad de stamina solicitada
    /// </summary>
    /// <param name="staminaConsumed"></param>
    public void ConsumeStamina(float staminaConsumed) {
        // Si se dispone de la cantidad de man? necesaria, se consume
        if (currentStamina > 0) {

            currentStamina -= staminaConsumed;
            if (currentStamina < 0) currentStamina = 0;

            
        }
        restoreTimer = coolDown;
    }

    /// <summary>
    /// Comprueba la stamina disponible
    /// </summary>
    /// <param name="staminaChecked"></param>
    /// <returns></returns>
    public bool CheckStamina(float staminaChecked) {
        return currentStamina >= staminaChecked;
    }


    public void RestoreStaminaOverTime() {
        if (CheckStamina(maxStamina)) {
            return;
        }
        staminaBar.fillAmount += staminaRestoration / WaitTime * Time.deltaTime;

        currentStamina = staminaBar.fillAmount * 100;
    }

    public void InfinteStamina()
    {
        currentStamina = 1000000000;
    }
    #endregion
}
