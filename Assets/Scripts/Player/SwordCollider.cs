using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SwordCollider : MonoBehaviour {

    public PlayerAttack playerAttack;
    public string target;
    public string target2;
    public PlayerController controller;
    public AudioClip hitClip;
    public float intensityCamShake;
    public float timeCamShake;

    private void OnTriggerEnter(Collider other) {

        if (other.gameObject.CompareTag(target) || other.gameObject.CompareTag(target2))
        {
            //StartCoroutine(Hit());

            controller.audioSource.PlayOneShot(hitClip);

            other.gameObject.GetComponent<IDamageable>().TakeDamage(playerAttack.currentAttackDamage);

            //CameraShake.Instance.ShakeCamera(intensityCamShake, timeCamShake);

            Debug.Log("Hit");
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag(target) || other.gameObject.CompareTag(target2))
        {
            CameraShake.Instance.ShakeCamera(intensityCamShake, timeCamShake);

        }
    }

    /// <summary>
    /// Detiene el tiempo en los impactos de los ataques
    /// </summary>
    /// <returns></returns>
    IEnumerator Hit()
    {
        Debug.Log("Time");

        Time.timeScale = 0.1f;

        yield return new WaitForSeconds(0.01f);

        Time.timeScale = 1;
    }
}
