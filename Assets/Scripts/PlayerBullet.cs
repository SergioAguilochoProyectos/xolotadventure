using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour {
    #region Variables

    [Header("Bullet Settings")]
    public float bulletMaxRange = 15f;
    public int bulletDamage;

    public Rigidbody rigidbody;

    private Vector3 initialPosition;
    private Vector3 actualPosition;

    public PlayerAttack player;


    #endregion

    #region Unity Events
    // Start is called before the first frame update
    void Start() {
        initialPosition = transform.position;
        actualPosition = transform.position;
    }

    // Update is called once per frame
    void Update() {
        CheckBulletOutOfRange();
    }

    private void OnCollisionEnter(Collision collision) 
    {
        DestroyBullet();

        if (collision.collider.tag.ToString() == "Enemy") collision.transform.GetComponent<Enemy>().TakeDamage(bulletDamage); 
        if(collision.collider.tag.ToString() == "Boss") collision.transform.GetComponent<Boss>().TakeDamage(bulletDamage);
    }
    #endregion

    #region Methods
    /// <summary>
    /// Destruye la bala
    /// </summary>
    public void DestroyBullet() {
        // Se destruir� a si mismo
        Destroy(gameObject);
    }

    /// <summary>
    /// Comprueba la distancia que recorre la bala
    /// </summary>
    public void CheckBulletOutOfRange() {
        actualPosition = transform.position;
        // Si la distancia entre la posici�n actual y la inicial es superior al rango m�ximo de la bala se destruye
        if (Vector3.Distance(initialPosition, actualPosition) > bulletMaxRange) {
            DestroyBullet();
        }
    }
    #endregion
}
