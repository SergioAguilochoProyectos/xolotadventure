using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Reaction : MonoBehaviour
{
    [TextArea]
    // Descripci�n de la reacci�n, como nota para el editor
    public string description;
    // Tiempo de ejecuci�n de la reacci�n
    public float delay;
    private float delayCounter;

    [HideInInspector]
    // Se alimentar� de forma autom�tica, por lo que se oculta en el inspector, NO se puede editar
    public Interactable interactable;
    // Para controlar si la reacci�n deber� esperar una interacci�n por parte del usuario
    public bool waitFor;
    // Para indicar si es posible saltar la reacci�n
    public bool skipable = false;
    // Para controlar si el jugador ha solicitado realizar "skip"
    private bool skip = false;

    void Update()
    {

        if (Input.GetButtonDown("Skip") && (skipable || waitFor))
        {
            skip = true;
        }

    }

    /// <summary>
    /// Acci�n que se realizar� previo al delay
    /// </summary>
    protected virtual void React()
    {
        //aqu� ir� el c�digo de la reacci�n

    }
    /// <summary>
    /// Acci�n que se realizar� tras el delay
    /// </summary>
    protected virtual void PostReact()
    {
        //una vez terminada la reacci�n, se le dice al Interactable que inicie la siguiente (Esto es una Cola de interacci�n)

        interactable.NextReaction();
    }
    /// <summary>
    /// Corrutina que se encarga de ejecutar la reacci�n y el delay
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator DelayReact()
    {
        React();
        // Se reinicia el contador del bucle
        delayCounter = delay;
        // Se realiza el control de skip
        skip = false;

        // Se realiza la espera por el tiempo indicado, mientras el Delay est� por encima de 0, o que waitFor est� activo, o skip desactivado
        while ((delayCounter > 0 || waitFor) && !skip)
        {
            yield return new WaitForEndOfFrame();
            delayCounter -= Time.deltaTime;
        }
        // Se pasa al siguiente luego de terminar la espera
        PostReact();
    }
    /// <summary>
    /// Se ejecuta la reacci�n como corrutina
    /// </summary>
    public virtual void ExecuteReaction()
    {
        StartCoroutine(DelayReact());
    }
}
