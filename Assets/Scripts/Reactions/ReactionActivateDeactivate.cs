using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionActivateDeactivate : Reaction
{

    public GameObject target;

    public bool enable;

    protected override void React()
    {
        target.SetActive(enable);
    }
}
