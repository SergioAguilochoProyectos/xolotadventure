using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para buscar en los arrays
using System.Linq;
public class ReactionAddArrow : Reaction
{
    public int amount;

    private void Start()
    {
        PlayerAttack.instance.currentArrowLoad += amount;
    }
}

