using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionAddToInventory : Reaction
{
    public string itemName;

    protected override void React()
    {
        InventoryManager.instance.AddItemInventory(itemName);
    }
}
