using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionAnimation : Reaction
{
    //Animator del objeto que ser� animado
    public Animator target;
    //Nombre del trigger de la animaci�n a ejecutar
    public string triggerName;

    private void Start()
    {
        if (target == null) target = PlayerController.instance.anim;
    }

    //M�todo que ejecuta la reacci�n, con override pisa el m�todo heredado de Reaction
    protected override void React() //Solo se pueden hacer override de los "virtual", sobreescribe
    {
        //Ejecutamos la animaci�n
        target.SetTrigger(triggerName);

        PlayerController.instance.anim.SetTrigger(triggerName);
    }

}
