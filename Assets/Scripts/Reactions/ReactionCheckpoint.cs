using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionCheckpoint : Reaction
{
    [Header("Checkpoint")]
    public string startingPosition;

    protected override void React()
    {
        DataManager.instance.data.entrancePosition = startingPosition;

        DataManager.instance.data.playerHealth = PlayerHealth.instance.maxHealth;

        PlayerHealth.instance.canHeal = false;

        DataManager.instance.Save();
    }

}
