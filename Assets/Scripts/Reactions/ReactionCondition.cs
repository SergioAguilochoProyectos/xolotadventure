using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionCondition : Reaction
{

    public string conditionName;
    // Se indica el estado al que modificaremos la condición
    public bool conditionStatus;

    protected override void React()
    {
        // Se llama al DataManager para modificar la condición
        DataManager.instance.SetCondition(conditionName, conditionStatus);
    }
}
