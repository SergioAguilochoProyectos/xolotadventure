using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionIKTouch : Reaction
{
    //Punto de interacci�n de la IK
    public Transform interactionSpot;
    //Referencia al control de IK
    public IK_Interaction ikInteract;
    //ikGoal seleccionado para realizar la animaci�n
    public AvatarIKGoal ikGoal;

    protected override void React()
    {
        //Se inicia la duraci�n en la base del valor del delay

        //Si delay es 0, si es TRUE ikInteract.movementDuration, si es FALSE delay(return)
        //delay = (delay == 0) ? ikInteract.movementDuration : delay;
        if (delay == 0)
        {
            delay = ikInteract.movementDuration;
        }
        else
        {
            ikInteract.movementDuration = delay;
        }
        //Asignamos los par�metros delikInteract
        ikInteract.target = interactionSpot;
        ikInteract.ikGoal = ikGoal;

        ikInteract.StartMovement();
    }

}
