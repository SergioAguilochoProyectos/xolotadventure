using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionInstancePrefab : Reaction
{

    public GameObject prefab;

    public Transform parent;
    protected override void React()
    {
        Instantiate(prefab, parent);
    }

}
