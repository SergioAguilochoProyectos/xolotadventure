using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionNextMission : Reaction
{
    [Header("Mission")]
    public string missionID;
    public int amount;

    protected override void React()
    {
        MissionManager.instance.CheckMission(missionID, amount);
    }

}
