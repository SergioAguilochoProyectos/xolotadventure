using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionOpenShop : Reaction
{
    public Animator playerAnim;

    protected override void React()
    {
        if (GameManager.instance.canBuy == false)
        {
            GameManager.instance.Shop();
        }
        PlayerController.instance.canMove = false;

        GameManager.instance.cameraFollow.enabled = false;

        playerAnim.SetFloat("Forward", 0);
        playerAnim.SetFloat("Right", 0);
    }
    protected override void PostReact()
    {
        base.PostReact();
    }
}
