using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionRemoveFromInventory : Reaction
{
    public string itemName;
    //public string itemName2;

    protected override void PostReact()
    {
        InventoryManager.instance.RemoveItemInventory(itemName);

        Debug.Log("Me ejecuto SOLO");

        //StartCoroutine(itemNameCoroutine());
    }
    /*private IEnumerator itemNameCoroutine()
    {
        itemName = "";

        yield return new WaitForSeconds(1f);

        itemName = itemName2;
    }*/
}
