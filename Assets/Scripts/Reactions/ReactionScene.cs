using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionScene : Reaction
{
    // Nombre de la escena a cargar
    public string nextScene;
    // Nombre de la pr�xima posici�n
    public string nextEntrancePosition;

    protected override void React()
    {
        //Llamamos al singleton que realizar� el cambio de escena
        SceneController.instance.FadeAndLoadScene(nextScene, nextEntrancePosition);
    }
}
