using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionSound : Reaction
{
    public AudioSource audioSource;
    public AudioClip audioClip;

    protected override void React()
    {
        audioSource.PlayOneShot(audioClip);
    }

}
