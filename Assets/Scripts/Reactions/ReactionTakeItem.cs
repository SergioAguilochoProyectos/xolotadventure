using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Para buscar en los arrays
using System.Linq;
public class ReactionTakeItem : Reaction
{

    private InteractableItem currentItem;
    public GameObject item;
    public int amount;

    private void Start()
    {     
        currentItem = GetComponentInParent<InteractableItem>();      
    }

    protected override void React()
    {
        Debug.Log("Pick");
        PlayerAttack.instance.currentArrowLoad += amount;
        PlayerController.instance.canInteract = false;
        StartCoroutine(PickupItem());
    }
    protected override void PostReact()
    {
        // Ocultamos el texto mostrado
        TextManager.instance.HideMessage();
        //base.PostReact(); // Se pasa a la siguiente reacci�n
    }
    /// <summary>
    /// Realiza la recuperaci�n del objeto
    /// </summary>
    private IEnumerator PickupItem()
    {
        BinaryItem result = DataManager.instance.data.allItems.Where(i => i.name == currentItem.itemName).FirstOrDefault();

        if(result != null)
        {
            if (InventoryManager.instance.AddItemInventory(currentItem.itemName))
            {
                // Si hab�a espacio para recoger el objeto, se actualiza AllItem, indicando
                // que el objeto ha sido recogido
                if (currentItem.itemName != "Arrow")
                {
                    yield return new WaitForSeconds(2f);

                    result.picked = true;

                    if(currentItem.itemName == "Infernal")
                    {
                        DataManager.instance.SetCondition("fireCampFinished", true);
                    }
                }
                // Se desactiva el objeto al recogerlo
                currentItem.gameObject.SetActive(false);
            }
            else
            {
                TextManager.instance.DisplayMessage(TranslateManager.instance.GetString("inventory_full"), Color.white, "");
            }
        }
        Debug.LogWarning("El nombre del objeto no existe: " + currentItem.itemName);
    }
    
}

