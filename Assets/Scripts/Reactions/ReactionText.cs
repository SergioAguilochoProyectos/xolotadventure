using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactionText : Reaction
{
    [Header("Texto")]
    public string textID;
    public Color textColor = Color.white;
    public string characterName;
    public float letterReadTime = 0.15f;

    protected override void React()
    {
        TextManager.instance.DisplayMessage(TranslateManager.instance.GetString(textID), textColor, characterName);

        delay = (delay == 0) ? TranslateManager.instance.GetString(textID).Length * letterReadTime : delay;

        PlayerHealth.instance.canHeal = false;
    }
    protected override void PostReact()
    {
        // Le pedimos al TextManager, que oculte el texto
        TextManager.instance.HideMessage();

        base.PostReact();
    }
}
