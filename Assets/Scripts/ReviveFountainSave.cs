using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReviveFountainSave : MonoBehaviour
{
    public string startingPosition;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            DataManager.instance.data.entrancePosition = startingPosition;

            DataManager.instance.Save();
        }
    }
}
