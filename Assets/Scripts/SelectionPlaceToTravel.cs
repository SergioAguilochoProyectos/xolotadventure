using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SelectionPlaceToTravel : MonoBehaviour, ISelectHandler, IDeselectHandler
{
    public Sprite[] placeSelection;
    public AudioSource audioSource;
    public AudioClip buttonAudio;
    public Image selectionImage;

    private void Start()
    {
        selectionImage.sprite = placeSelection[0];
    }
    public void OnSelect(BaseEventData eventData)
    {
        selectionImage.sprite = placeSelection[1];

        audioSource.PlayOneShot(buttonAudio);
    }

    public void OnDeselect(BaseEventData eventData)
    {
        selectionImage.sprite = placeSelection[0];
    }


}
