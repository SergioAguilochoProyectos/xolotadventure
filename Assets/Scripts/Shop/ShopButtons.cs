using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
public class ShopButtons : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler
{
    private Button shopItem;
    public static ShopButtons instance;



    private int moneyToDiscount = 100;
    private int moneyToDiscountWaterGem = 1400;
    private int moneyToDiscountArrow = 50;

    public Action OnBuy;
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }
    void Start()
    {
        shopItem = GetComponent<Button>();
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        shopItem.image.color = Color.yellow;
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        shopItem.image.color = Color.white;
    }

    public void OnSelect(BaseEventData eventData)
    {
        if (Input.GetJoystickNames().Length > 0)
        {
            shopItem.image.color = Color.yellow;
        }
        //Cuando se selecciona en la tienda un bot�n almacena su nombre
        ShopItemBuy.instance.buttonSelectedName = EventSystem.current.currentSelectedGameObject.name;
    }
    public void OnDeselect(BaseEventData data)
    {
        if (Input.GetJoystickNames().Length > 0)
        {
            shopItem.image.color = Color.white;
        }
    }
}
