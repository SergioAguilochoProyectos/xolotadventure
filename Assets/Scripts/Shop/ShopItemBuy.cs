using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ShopItemBuy : MonoBehaviour
{
    public Button[] itemButtons;
    public AudioSource audioSource;
    public AudioClip moneyExchange;

    public static ShopItemBuy instance;

    public string buttonSelectedName;

    public Button waterGemShop;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    public void PayForItem()
    {
        int arrowPrice = 20;
        int potionPrice = 100;
        int blueGemPrice = 1000;

        if (DataManager.instance.data.moneyCount > 0)
        {
            if (buttonSelectedName == "Item1")
            {
                // Si la cantidad de dinero - el valor del producto es mayor o igual a 0 podr� descontarlo
                if(DataManager.instance.data.moneyCount - arrowPrice >= 0)
                {
                    DataManager.instance.data.moneyCount -= arrowPrice;
                    InventoryManager.instance.AddItemInventory("Arrow");
                }
            }
            if (buttonSelectedName == "Item2")
            {
                if (DataManager.instance.data.moneyCount - potionPrice >= 0)
                {
                    DataManager.instance.data.moneyCount -= potionPrice;

                    InventoryManager.instance.AddItemInventory("Potion");
                }
            }
            if (buttonSelectedName == "Item3")
            {
                if (DataManager.instance.data.moneyCount - blueGemPrice >= 0)
                {
                    DataManager.instance.data.moneyCount -= blueGemPrice;

                    InventoryManager.instance.AddItemInventory("Celeste");

                    waterGemShop.enabled = false;
                }
            }

            audioSource.PlayOneShot(moneyExchange, 0.5f);

            DataManager.instance.Save();
        }
        else
        {
            DataManager.instance.data.moneyCount = 0;
        }

    }


}
