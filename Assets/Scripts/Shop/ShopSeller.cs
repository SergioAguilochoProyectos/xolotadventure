using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ShopSeller : MonoBehaviour
{
    public Animator playerAnim;
    public Animator sellerAnim;

    public static ShopSeller instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (Input.GetButtonDown("Interact"))
            {
                if(GameManager.instance.canBuy == false)
                {
                    GameManager.instance.Shop();
                }

                PlayerController.instance.canMove = false;

                GameManager.instance.cameraFollow.enabled = false;

                playerAnim.SetFloat("Forward", 0);
                playerAnim.SetFloat("Right", 0);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Utils.ActiveCanvasGroup(GameManager.instance.canvas[7], false);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}
