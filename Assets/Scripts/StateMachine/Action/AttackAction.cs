using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Actions/Attack")]
public class AttackAction : StateAction {

    public override void Act(StateController controller) {
        if (DataManager.instance.data.playerHealth > 0f) {
            Attack(controller);
        }
    }

    /// <summary>
    /// Si est� dentro del rango de ataque disparar� su arma
    /// </summary>
    /// <param name="controller"></param>
    public void Attack(StateController controller) {
        if (controller.navMeshAgent.remainingDistance <= controller.enemyStats.attackRange) {
            // Dispara el arma haciendo uso del action
            controller.OnAttack?.Invoke();
        }
    }
}
