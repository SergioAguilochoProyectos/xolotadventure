using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Actions/Chase")]
public class ChaseAction : StateAction {

    public override void Act(StateController controller) {
        Chase(controller);
    }

    /// <summary>
    /// Desplaza a la IA hasta la posici�n del target
    /// </summary>
    /// <param name="controller"></param>
    private void Chase(StateController controller) {
        if (controller.isDamaged) {
            controller.isDamaged = false;
            controller.navMeshAgent.destination = controller.player.position;
            controller.target = controller.player;

        }
        // Si hay un target fijado
        if (controller.target != null) {
            // Se fija la �ltima posici�n del target como destino
            controller.navMeshAgent.destination = controller.target.position;
            // Se inicia el movimiento
            controller.navMeshAgent.isStopped = false;

            if (Vector3.Distance(controller.navMeshAgent.destination, controller.transform.position) <= controller.enemyStats.attackRange) {
                // Se indica al animator que estar� en movimiento pero no corriendo
                controller.animator.SetBool("Moving", false);
                controller.animator.SetBool("Running", false);
                // Se cambia su velocidad para la persecuci�n
                controller.navMeshAgent.speed = 0;
                // Se rota el transform para que apunte al jugador
                ApplyRotation(controller);
            } else {
                // Se indica al animator que estar� en movimiento pero no corriendo
                controller.animator.SetBool("Moving", true);
                controller.animator.SetBool("Running", true);
                // Se cambia su velocidad para la persecuci�n
                controller.navMeshAgent.speed = controller.enemyStats.chaseSpeed;
            }
        }

        /*
         * Si llega hasta la �ltima posici�n en la que avist� al objetivo, sin que haya saltado otro estado (ataque, etc...) 
         * significar� que no est� viendo al objetivo y que lo ha perdido
         */
        if (controller.navMeshAgent.remainingDistance <= controller.navMeshAgent.stoppingDistance) {
            controller.target = null;
        }
    }

    private void ApplyRotation(StateController controller) {
        // Se determina la direcci�n a la que rotar
        Vector3 targetDirection = controller.navMeshAgent.destination - controller.transform.position;
        // Se aplica la rotaci�n de forma flu�da
        Vector3 newDirection = Vector3.RotateTowards(controller.transform.forward, targetDirection, Time.deltaTime * 2, 0.0f);
        controller.transform.rotation = Quaternion.LookRotation(newDirection);
    }
}
