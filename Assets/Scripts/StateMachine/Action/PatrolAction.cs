using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Actions/Patrol")]
public class PatrolAction : StateAction 
{
    public bool randomPatrol = false;

    public override void Act(StateController controller) {
        Patrol(controller);
    }

    /// <summary>
    /// Realiza las acciones de cambio de waypoint y gesti?n del navmesh para la patrulla
    /// </summary>
    /// <param name="controller"></param>
    private void Patrol(StateController controller) {
        // Se indica al animator que estar� en movimiento pero no corriendo
        controller.animator.SetBool("Moving", true);

        controller.animator.SetBool("Running", false);
        // Se recupera el siguiente waypoint y se establece como destino
        controller.navMeshAgent.destination = controller.wayPointList[controller.nextWaypoint].position;
        // Se inicia el movimiento hacia el destino
        controller.navMeshAgent.isStopped = false;
        // Se asigna la velocidad de patrulla
        controller.navMeshAgent.speed = controller.enemyStats.patrolSpeed;

        // Si la distancia al objetivo es menor al stopping distance se considera haber llegado al waypoint
        if (controller.navMeshAgent.remainingDistance <= controller.navMeshAgent.stoppingDistance &&
            !controller.navMeshAgent.pathPending) {
            if (randomPatrol) {
                // Elige un punto aleatorio de la lista
                controller.nextWaypoint = Random.Range(0, controller.wayPointList.Count);
            } else {
                controller.nextWaypoint = (controller.nextWaypoint + 1) % controller.wayPointList.Count;
            }
        }
    }
}
