using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Actions/ReturnToWaypoint")]
public class ReturnToWaypointAction : StateAction {

    // Velocidad de giro
    public float turnSpeed = 180f;
    // Tiempo que durar� el estado de perdido
    public float lostDuration = 4f;

    public override void Act(StateController controller) {
        Lost(controller);
    }

    /// <summary>
    /// Rota sobre si mismo en busca del objetivo
    /// </summary>
    /// <param name="controller"></param>
    private void Lost(StateController controller) {
        controller.target = null;
        controller.navMeshAgent.destination = controller.lastPosition;
    }
}
