using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StateAction : ScriptableObject
{
    // Todas las clases que hereden de esta tendr�n obligatoriamente este m�todo 
    public abstract void Act(StateController controller);
}
