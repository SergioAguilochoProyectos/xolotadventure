using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Decision : ScriptableObject
{
    /// <summary>
    /// Toma la decisi�n en base a la l�gica aplicada y devuelve un bool con el resultado
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    public abstract bool Decide(StateController controller);
}
