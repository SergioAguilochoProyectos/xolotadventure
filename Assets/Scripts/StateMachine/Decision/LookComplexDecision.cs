using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Decisions/LookComplex")]
public class LookComplexDecision : Decision
{
    // Para reducir la b�squeda inicial
    public LayerMask targetLayer;
    // Para localizar al aut�ntico objetivo
    public string targetTag;

    public bool debug;

    public override bool Decide(StateController controller) {
        if (debug) DebugFieldOfView(controller);
        return Look(controller);
    }

    /// <summary>
    /// Localiza al objetivo
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    private bool Look(StateController controller) {

        RaycastHit hit;
        Collider[] cols = Physics.OverlapSphere(controller.transform.position,
                                                controller.enemyStats.reach,
                                                targetLayer);

        // Si el n�mero de colisiones es superior a 0 significar� que ha detectado objetivos posibles
        if (cols.Length > 0) {
            foreach (Collider col in cols) {
                // Evalua si el posible objetivo se encuentra dentro del �ngulo de visi�n
                if (Vector3.Angle((col.transform.position - controller.eye.position),
                                    controller.eye.forward) < controller.enemyStats.fieldOfView / 2) {
                    // Se a�ade un offset de una unidad en el eje Y para que no apunte hacia el suelo
                    Vector3 offSetCol = new Vector3(col.transform.position.x, col.transform.position.y + 1f, col.transform.position.z);
                    // Verifica que exista visi�n directa con el target
                    if (Physics.Raycast(controller.eye.position,
                                        offSetCol - controller.eye.position,
                                        out hit,
                                        controller.enemyStats.reach)) {
                        // Si hay visi�n directa
                        if (hit.collider.CompareTag(targetTag)) {
                            // Fija el objetivo
                            controller.target = hit.transform;
                            controller.lastPosition = controller.transform.position;
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private void DebugFieldOfView(StateController controller) {
        Debug.DrawRay(controller.eye.position,
                        (Quaternion.AngleAxis(controller.enemyStats.fieldOfView / 2, controller.eye.up) *
                        controller.eye.forward) * controller.enemyStats.reach,
                        Color.red);

        Debug.DrawRay(controller.eye.position,
                        (Quaternion.AngleAxis(-controller.enemyStats.fieldOfView / 2, controller.eye.up) *
                        controller.eye.forward) * controller.enemyStats.reach,
                        Color.red);

        Debug.DrawRay(controller.eye.position,
                        (Quaternion.AngleAxis(controller.enemyStats.fieldOfView / 2, controller.eye.right) *
                        controller.eye.forward) * controller.enemyStats.reach,
                        Color.red);

        Debug.DrawRay(controller.eye.position,
                        (Quaternion.AngleAxis(-controller.enemyStats.fieldOfView / 2, controller.eye.right) *
                        controller.eye.forward) * controller.enemyStats.reach,
                        Color.red);
    }

}
