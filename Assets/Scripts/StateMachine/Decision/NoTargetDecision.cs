using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Decisions/NoTarget")]
public class NoTargetDecision : Decision {
    public override bool Decide(StateController controller) {
        return CheckTarget(controller);
    }

    /// <summary>
    /// Eval�a si deja de tener objetivo
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    private bool CheckTarget(StateController controller) {
        return (controller.target == null);
    }
}
