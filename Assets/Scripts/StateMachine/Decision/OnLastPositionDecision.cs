using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Decisions/OnLastPosition")]
public class OnLastPositionDecision : Decision {

    public override bool Decide(StateController controller) {
        return OnLastPosition(controller);
    }

    /// <summary>
    /// Eval�a si deja de tener objetivo
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    private bool OnLastPosition(StateController controller) {
        return (controller.transform.position - controller.lastPosition).magnitude < 0.1f;
    }
}
