using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "AISystem/Decisions/OutOfRange")]
public class OutOfRangeDecision : Decision
{
    public override bool Decide(StateController controller) {
        return CheckRange(controller);
    }

    /// <summary>
    /// Eval�a si deja de tener objetivo
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    private bool CheckRange(StateController controller) {
        return (Vector3.Distance(controller.transform.position, controller.lastPosition) < 20);
    }
}
