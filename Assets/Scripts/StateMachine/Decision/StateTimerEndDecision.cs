using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Decisions/StateTimerEnd")]
public class StateTimerEndDecision : Decision {
    public override bool Decide(StateController controller) {
        return CheckTimer(controller);
    }

    /// <summary>
    /// Verifica si ha terminado la cuenta atr�s
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    private bool CheckTimer(StateController controller) {
        return (controller.stateTimer <= 0);
    }
}
