using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/Decisions/TakeDamage")]
public class TakeDamageDecision : Decision {
    public override bool Decide(StateController controller) {
        return CheckDamageDealer(controller);
    }

    /// <summary>
    /// Eval�a si deja de tener objetivo
    /// </summary>
    /// <param name="controller"></param>
    /// <returns></returns>
    private bool CheckDamageDealer(StateController controller) {
        return (controller.isDamaged);
    }
}
