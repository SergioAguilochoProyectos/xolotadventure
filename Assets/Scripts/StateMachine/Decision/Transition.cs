using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Transition
{
    // Decisi�n a evaluar
    public Decision decision;
    // Estado al que cambiar en caso de que se cumpla la decisi�n
    public State trueState;
    // Estado al que cambiar en caso de que no se cumpla la decisi�n
    public State falseState;
}
