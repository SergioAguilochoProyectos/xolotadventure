using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AISystem/State")]
public class State : ScriptableObject
{
    // Listado de acciones que realizar� el estado
    public StateAction[] actions;
    // Listado de transiciones a evaluar
    public Transition[] transitions;

    // Distancia de parada del estado
    public float stoppingDistance = 2f;

    /// <summary>
    /// Actualiza el estado
    /// </summary>
    /// <param name="controller"></param>
    public void UpdateState(StateController controller) {
        DoActions(controller);
        CheckTransition(controller);
    }

    /// <summary>
    /// Realiza todas las acciones del estado.
    /// </summary>
    /// <param name="controller"></param>
    public void DoActions(StateController controller) {
        foreach (StateAction action in actions) {
            action.Act(controller);
        }
    }

    /// <summary>
    /// Eval�a las decisiones de las transiciones
    /// </summary>
    /// <param name="controller"></param>
    public void CheckTransition(StateController controller) {
        foreach (Transition transition in transitions) {
            bool decisionSucceded = transition.decision.Decide(controller);
            //Debug.Log("[LOG STATE] " + controller.currentState.name + " (" + transition.decision.name + ")" + " - Resultado: " + decisionSucceded + " [TRUE: " + transition.trueState + "] [False: " + transition.falseState + "]");
            if (decisionSucceded) {
                // Si el resultado es positivo se cambia al estado configurado para el resultado true
                controller.TransitionToState(transition.trueState);
            } else {
                controller.TransitionToState(transition.falseState);
            }

            /*
             * Si el valor ha cambiado significa que se ha producido una transici�n por lo que se cortar�n 
             * otras posibles transiciones que puedan romper la l�gica configurada
             */
            if (controller.currentState != this) break;
        }
    }
}
