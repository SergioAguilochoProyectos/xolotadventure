using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StateController : WeaponHolder
{
    public bool debugMode;

    [Header("State Machine")]
    // Estado actual de la m�quina de estados
    public State currentState;

    // Referencia al NavMeshAgent
    [HideInInspector]
    public NavMeshAgent navMeshAgent;
    // Referencia al Animator
    [HideInInspector]
    public Animator animator;

    [Header("Patrol Settings")]
    // Listado de waypoints disponibles para la patrulla
    public List<Transform> wayPointList;
    // Siguiente waypoint al que se desplaza
    public int nextWaypoint;

    // Para desactivar la IA en caso de ser necesario
    public bool aiActive = true;

    // Estad?sticas del enemigo
    public EnemyStats enemyStats;
    // Posici�n y orientaci�n del ojo
    public Transform eye;
    // Transform del objetivo
    public Transform target;
    // Posici�n origen movimiento
    public Vector3 lastPosition;

    [HideInInspector]
    // Contador para ser utilizado por los estados
    public float stateTimer;
    [HideInInspector]
    public bool timerCounting;
    [HideInInspector]
    public bool isDamaged;

    public Transform player;

    // Referencia al componente que gestiona la vida del enemigo
    public Enemy enemyHealth;

    private void Awake() {
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
    }

    private void Update() {
        // Si no est? activa la IA se sale del m?todo sin hacer nada
        if (!aiActive) return;
        // Realiza el update del State indicando que somos el controller a utilizar
        currentState.UpdateState(this);
    }


    /// <summary>
    /// Cambia la m?quina de estados al estado indicado
    /// </summary>
    /// <param name="nextState"></param>
    public void TransitionToState(State nextState) {
        // Si el pr?ximo estado es distinto al actual
        if (nextState != currentState) {
            // Se hace efectivo el cambio de estado
            currentState = nextState;
            // Se resetea el contador de temporizador en caso de cambiar de estado
            stateTimer = 0f;
            timerCounting = false;
            // Ajusta el stopping distance para el estado
            navMeshAgent.stoppingDistance = nextState.stoppingDistance;

            enemyHealth.healingTimer = Time.time + enemyStats.healingTick;
        }
    }
}
