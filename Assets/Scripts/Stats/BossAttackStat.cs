using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Stats/Characters/BossAttackStat")]
public class BossAttackStat : ScriptableObject
{
    public int damage;
}
