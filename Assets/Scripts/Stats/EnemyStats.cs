using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Stats/Characters/Enemy")]
public class EnemyStats : ScriptableObject
{
    [Header("Basic Stat Settings")]
    public int maxHealth;
    public int patrolSpeed;
    public int chaseSpeed;

    [Header("Basic Combat Settings")]
    public int attackDamage;
    public float attackRange;
    public float attackSpeed;
    public float healingTick;

    public float reach;
    public float fieldOfView;
}
