using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAttackCollider : MonoBehaviour
{
    public BossAttackStat stats;
    public string target;

    private void OnTriggerEnter(Collider other) {

        if (other.gameObject.CompareTag(target)) {

            other.gameObject.GetComponent<IDamageable>().TakeDamage(stats.damage);
        }
    }
}
