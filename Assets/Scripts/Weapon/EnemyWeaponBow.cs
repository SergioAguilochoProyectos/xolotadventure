using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeaponBow : MonoBehaviour
{
    public Bullet arrowPrefab;
    public Transform firePoint;
    public Transform sectionContainer;
    public EnemyStats stats;
    public string target;


    private float timer;

    private void Update() {
        UpdateTimer();
    }

    public void RangedAttack() {
        if (timer > stats.attackSpeed) {
            //audioSource.PlayOneShot(bowAttackSound);
            timer = Time.deltaTime;
            // Se instancia una nueva bala y se almacena como referencia de la secci�n actual
            Bullet bullet = Instantiate(arrowPrefab, firePoint.position, transform.rotation, sectionContainer);
            bullet.tag = target;
            bullet.damage = stats.attackDamage;
            bullet.rigidbody.AddRelativeForce(new Vector3(0f, 0f, 1800f));
        }
    }

    /// <summary>
    /// Actualiza el timer del fire rating
    /// </summary>
    private void UpdateTimer() {
        timer += Time.deltaTime;
    }
}
