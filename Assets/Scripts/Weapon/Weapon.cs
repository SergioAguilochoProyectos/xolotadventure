using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Stats/Weapons/Weapon")]
public class Weapon : ScriptableObject {
    [Header("Ranged Settings")]
    [Tooltip("Define si el arma es a distancia")]
    public bool isRanged;

    [Header("Basic Weapon Settings")]
    // Da�o del arma
    public int damage;
    // Alcance del arma
    public float range = 100f;
    // Fuerza del impacto del arma
    public float impactForce = 30f;
    // Tiempo entre ataques
    public float attackDelay = 0.5f;


}
