using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponHold : MonoBehaviour
{
    public Weapon weapon;

    public Animator animator;

    public WeaponHolder weaponHolder;
    // Tiempo en el que se puede realizar el siguiente ataque
    private float nextTimeToAttack = 0f;

    private void OnEnable() {
        if (weaponHolder != null) {
            weaponHolder.OnAttack += Attack;
        }
    }

    private void OnDisable() {
        if (weaponHolder != null) {
            weaponHolder.OnAttack -= Attack;
        }
    }

    public void Attack() {
        // Si no ha pasado el tiempo de delay entre ataques, no se permite atacar
        if (Time.time < nextTimeToAttack) return;

        // Se calcula el time para poder realizar el siguiente disparo
        nextTimeToAttack = Time.time + weapon.attackDelay;
        // Se ejecuta el trigger del animator
        animator.SetTrigger("Attack");
    }
}
